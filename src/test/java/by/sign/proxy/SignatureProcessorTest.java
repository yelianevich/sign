package by.sign.proxy;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.Security;
import java.security.interfaces.DSAPrivateKey;
import java.security.interfaces.DSAPublicKey;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.sign.proxy.actor.OriginalSigner;
import by.sign.proxy.actor.ProxySigner;
import by.sign.proxy.actor.Verifier;
import by.sign.proxy.artifact.Proxy;
import by.sign.proxy.artifact.SubstituteProxy;
import by.sign.proxy.artifact.Warrant;
import by.sign.proxy.key.DsaKeyManagement;
import by.sign.proxy.key.DsaKeyPairWrapper;

public class SignatureProcessorTest {

	@Before
	public void setUp() throws Exception {
		Security.addProvider(new BouncyCastleProvider());
	}

	@After
	public void tearDown() throws Exception {
		Security.removeProvider("BC");
	}

	@Test
	public void signAndVerifyByGeneratedKeys() {
		KeyPair keyPair = DsaKeyManagement.generateKeyPair();

		DsaSignatureProcessor processor = new DsaSignatureProcessor();
		byte[] content = { 1, 2, 3, 4, 5, 6, 8, 9, 10 };
		byte[] sign = processor.sign(keyPair.getPrivate(), content);
		assertThat(sign, is(notNullValue()));
		assertThat(sign.length, is(not(0)));

		boolean valid = processor.verify(keyPair.getPublic(), content, sign);

		assertThat(valid, is(true));

		byte[] anotherContent = { 1, 2, 3, 4, 5 };
		boolean notValid = processor.verify(keyPair.getPublic(), anotherContent, sign);

		assertThat(notValid, is(false));

		byte[] anotherSign = { 1, 2, 3, 4, 5 };
		notValid = processor.verify(keyPair.getPublic(), content, anotherSign);

		assertThat(notValid, is(false));
	}

	@Test
	public void signAndVerifyByProxyKeys() {
		DsaKeyPairWrapper keyPairWrapper = DsaKeyManagement.generateWrappedKeyPair();
		OriginalSigner originalSigner = new OriginalSigner(keyPairWrapper);
		Warrant warrant = Warrant.getOneYearWarrant();
		Proxy proxy = originalSigner.generateProxy(warrant);

		DsaKeyPairWrapper keyPairWrappedProxy = DsaKeyManagement.generateWrappedKeyPair();
		ProxySigner proxySigner = new ProxySigner(keyPairWrappedProxy);

		SubstituteProxy substituteProxy = proxySigner.generateSubstituteProxy(proxy);

		// test protected scheme
		BigInteger yp = Verifier.computeProtectedPublicKey(substituteProxy);
		DSAPublicKey publicKey = DsaKeyManagement.getPublicKey(yp);
		DSAPrivateKey privateKey = DsaKeyManagement.getProtectedPrivateKey(substituteProxy);

		DsaSignatureProcessor signer = new DsaSignatureProcessor();
		byte[] content = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		byte[] sign = signer.sign(privateKey, content);
		boolean valid = signer.verify(publicKey, content, sign);

		assertThat(valid, is(true));

		// test unprotected scheme
		yp = Verifier.computeUnprotectedPublicKey(proxy);
		publicKey = DsaKeyManagement.getPublicKey(yp);
		privateKey = DsaKeyManagement.getUnprotectedPrivateKey(proxy);

		sign = signer.sign(privateKey, content);
		valid = signer.verify(publicKey, content, sign);

		assertThat(valid, is(true));
	}

}
