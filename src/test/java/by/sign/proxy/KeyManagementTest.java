package by.sign.proxy;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.Security;
import java.security.interfaces.DSAPrivateKey;
import java.security.interfaces.DSAPublicKey;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.sign.proxy.actor.OriginalSigner;
import by.sign.proxy.actor.ProxySigner;
import by.sign.proxy.actor.Verifier;
import by.sign.proxy.artifact.Proxy;
import by.sign.proxy.artifact.SubstituteProxy;
import by.sign.proxy.artifact.Warrant;
import by.sign.proxy.key.DsaKeyManagement;
import by.sign.proxy.key.DsaKeyPairWrapper;

public class KeyManagementTest {

	@Before
	public void setUp() throws Exception {
		Security.addProvider(new BouncyCastleProvider());
	}

	@After
	public void tearDown() throws Exception {
		Security.removeProvider("BC");
	}

	@Test
	public void shouldCreateKeyPair() {
		/*DSAParametersGenerator dsaParametersGenerator = new DSAParametersGenerator();
		dsaParametersGenerator.init(1024, 100, new SecureRandom());
		DSAParameters dsaParams = dsaParametersGenerator.generateParameters();
		System.out.println(dsaParams.getG().toString(16));
		System.out.println(dsaParams.getP().toString(16));
		System.out.println(dsaParams.getQ().toString(16));*/

		KeyPair keyPair = DsaKeyManagement.generateKeyPair();
		DsaKeyPairWrapper dsaKeyPairWrapper = new DsaKeyPairWrapper(keyPair);
		System.out.println(dsaKeyPairWrapper.getX().toString(16));
		System.out.println(dsaKeyPairWrapper.getY().toString(16));
	}

	@Test
	public void shouldCreatePublicKey() {
		DsaKeyPairWrapper keyPairWrapper = DsaKeyManagement.generateWrappedKeyPair();
		OriginalSigner originalSigner = new OriginalSigner(keyPairWrapper);
		Warrant warrant = Warrant.getOneYearWarrant();
		Proxy proxy = originalSigner.generateProxy(warrant);

		DsaKeyPairWrapper keyPairWrappedProxy = DsaKeyManagement.generateWrappedKeyPair();
		ProxySigner proxySigner = new ProxySigner(keyPairWrappedProxy);

		SubstituteProxy substituteProxy = proxySigner.generateSubstituteProxy(proxy);

		BigInteger yp = Verifier.computeProtectedPublicKey(substituteProxy);
		
		DSAPublicKey dsaPublicKey = DsaKeyManagement.getPublicKey(yp);
		assertThat(dsaPublicKey.getY(), is(yp));
	}

	@Test
	public void shouldCreatePrivateKeyFromProxy() {
		DsaKeyPairWrapper keyPairWrapper = DsaKeyManagement.generateWrappedKeyPair();
		OriginalSigner originalSigner = new OriginalSigner(keyPairWrapper);
		Warrant warrant = Warrant.getOneYearWarrant();
		Proxy proxy = originalSigner.generateProxy(warrant);

		DsaKeyPairWrapper keyPairWrappedProxy = DsaKeyManagement.generateWrappedKeyPair();
		ProxySigner proxySigner = new ProxySigner(keyPairWrappedProxy);

		SubstituteProxy substituteProxy = proxySigner.generateSubstituteProxy(proxy);
		DSAPrivateKey dsaPrivateKey = DsaKeyManagement.getProtectedPrivateKey(substituteProxy);

		assertThat(dsaPrivateKey.getX(), is(substituteProxy.getSigmSubstitute()));

		dsaPrivateKey = DsaKeyManagement.getUnprotectedPrivateKey(proxy);

		assertThat(dsaPrivateKey.getX(), is(proxy.getSigm()));
	}

}
