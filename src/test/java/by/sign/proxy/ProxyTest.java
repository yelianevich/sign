package by.sign.proxy;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.security.Security;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.IntStream;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.sign.proxy.actor.OriginalSigner;
import by.sign.proxy.actor.ProxySigner;
import by.sign.proxy.actor.ProxyThresholdSigner;
import by.sign.proxy.actor.Verifier;
import by.sign.proxy.actor.VerifyStatus;
import by.sign.proxy.artifact.Group;
import by.sign.proxy.artifact.Proxy;
import by.sign.proxy.artifact.SubstituteProxy;
import by.sign.proxy.artifact.ThresholdProxySignature;
import by.sign.proxy.artifact.Warrant;
import by.sign.proxy.key.DsaKeyManagement;
import by.sign.proxy.key.DsaKeyPairWrapper;

public class ProxyTest {

	@Before
	public void setUp() throws Exception {
		Security.addProvider(new BouncyCastleProvider());
	}

	@After
	public void tearDown() throws Exception {
		Security.removeProvider("BC");
	}

	@Test
	public void shouldGenerateValidProxy() {
		DsaKeyPairWrapper keyPairWrapper = DsaKeyManagement.generateWrappedKeyPair();
		OriginalSigner originalSigner = new OriginalSigner(keyPairWrapper);

		Warrant warrant = Warrant.getOneYearWarrant();

		Proxy proxy = originalSigner.generateProxy(warrant);
		DsaKeyPairWrapper keyPairWrappedProxy = DsaKeyManagement.generateWrappedKeyPair();
		ProxySigner proxySigner = new ProxySigner(keyPairWrappedProxy);
		boolean isValidProxy = proxySigner.verifyProxy(proxy);

		assertTrue(isValidProxy);
	}

	@Test
	public void proxySignerShouldGenerateNotNullSubstituteProxy() {
		DsaKeyPairWrapper keyPairWrapper = DsaKeyManagement.generateWrappedKeyPair();
		OriginalSigner originalSigner = new OriginalSigner(keyPairWrapper);
		Warrant warrant = Warrant.getOneYearWarrant();
		Proxy proxy = originalSigner.generateProxy(warrant);

		DsaKeyPairWrapper keyPairWrappedProxy = DsaKeyManagement.generateWrappedKeyPair();
		ProxySigner proxySigner = new ProxySigner(keyPairWrappedProxy);

		SubstituteProxy substituteProxy = proxySigner.generateSubstituteProxy(proxy);

		assertThat(substituteProxy, is(notNullValue()));
	}

	@Test
	public void newUnprotectedPublicKeyIsCompatibleWithProxyPrivateKey() {
		DsaKeyPairWrapper keyPairWrapper = DsaKeyManagement.generateWrappedKeyPair();
		OriginalSigner originalSigner = new OriginalSigner(keyPairWrapper);
		Warrant warrant = Warrant.getOneYearWarrant();
		Proxy proxy = originalSigner.generateProxy(warrant);

		DsaKeyPairWrapper keyPairWrappedProxy = DsaKeyManagement.generateWrappedKeyPair();
		ProxySigner proxySigner = new ProxySigner(keyPairWrappedProxy);

		SubstituteProxy substituteProxy = proxySigner.generateSubstituteProxy(proxy);

		BigInteger yp = Verifier.computeUnprotectedPublicKey(substituteProxy);
		BigInteger ypComputed = ParamsHolder.G.modPow(substituteProxy.getSigm(), ParamsHolder.P);

		assertThat(ypComputed, is(yp));
	}

	@Test
	public void newProtectedPublicKeyIsCompatibleWithProxyPrivateKey() {
		DsaKeyPairWrapper keyPairWrapper = DsaKeyManagement.generateWrappedKeyPair();
		OriginalSigner originalSigner = new OriginalSigner(keyPairWrapper);
		Warrant warrant = Warrant.getOneYearWarrant();
		Proxy proxy = originalSigner.generateProxy(warrant);

		DsaKeyPairWrapper keyPairWrappedProxy = DsaKeyManagement.generateWrappedKeyPair();
		ProxySigner proxySigner = new ProxySigner(keyPairWrappedProxy);

		SubstituteProxy substituteProxy = proxySigner.generateSubstituteProxy(proxy);

		BigInteger yp = Verifier.computeProtectedPublicKey(substituteProxy);
		BigInteger ypComputed = ParamsHolder.G.modPow(substituteProxy.getSigmSubstitute(), ParamsHolder.P);

		assertThat(ypComputed, is(yp));
	}

	@Test
	public void shouldGenerateValidProxyShares() {
		DsaKeyPairWrapper keyPairWrapper = DsaKeyManagement.generateWrappedKeyPair();
		OriginalSigner originalSigner = new OriginalSigner(keyPairWrapper);

		Group thresholdGroup = new Group();
		List<ProxyThresholdSigner> groupMembers = generateGroupMembers(thresholdGroup, 10);
		thresholdGroup.setMembers(groupMembers);
		thresholdGroup.setThreshold(5);

		originalSigner.setProxySharesToGroup(thresholdGroup, Warrant.getOneYearWarrant());
		for (ProxyThresholdSigner proxySigner : groupMembers) {
			boolean isValidProxyShare = proxySigner.verifyProxyShare();
			int shareIndex = proxySigner.getProxyShare().getProxyShareIndex();
			String errMsg = String.format("Proxy share is not valid for %s user", shareIndex);
			assertThat(errMsg, isValidProxyShare, is(true));
		}
	}

	@Test
	public void thresholdGroupCanGenerateProxyUnprotectedSignature() {
		DsaKeyPairWrapper keyPairWrapper = DsaKeyManagement.generateWrappedKeyPair();
		OriginalSigner originalSigner = new OriginalSigner(keyPairWrapper);

		Group group = new Group();
		List<ProxyThresholdSigner> members = generateGroupMembers(group, 10);
		group.setMembers(members);
		group.setSignPublishers(members.subList(0, 5));
		group.setThreshold(5);

		// 0
		originalSigner.setProxySharesToGroup(group, Warrant.getOneYearWarrant());
		for (ProxyThresholdSigner proxySigner : members) {
			boolean isValidProxyShare = proxySigner.verifyProxyShare();
			int shareIndex = proxySigner.getIndexInGroup();
			String errMsg = String.format("Proxy share is not valid for %s user", shareIndex);
			assertThat(errMsg, isValidProxyShare, is(true));
		}
		List<ProxyThresholdSigner> signMembers = group.getSignPublishers();
		// 1
		signMembers.forEach(ProxyThresholdSigner::shareSecretToGroup);
		List<Integer> notValidShare = signMembers.stream()
				.map(ProxyThresholdSigner::verifyGroupShares)
				.filter(list -> !list.isEmpty())
				.findFirst()
				.orElse(Collections.emptyList());
		String errMsg = String.format("Proxy shares from ids: [%s] not valid", StringUtils.join(notValidShare, ", "));

		assertThat(errMsg, notValidShare, is(empty()));

		byte[] msg = "Sign me".getBytes(ParamsHolder.SIGN_TYPE_CHARSET);
		signMembers.forEach(ProxyThresholdSigner::computeGroupShares);
		signMembers.forEach(signer -> signer.signWithProxyShare(msg));
		Consumer<VerifyStatus> print = System.out::println;
		Consumer<VerifyStatus> assertResult = r -> assertThat(r.isValid(), is(true));
		signMembers.forEach(s -> {
			List<VerifyStatus> status = s.verifySignedMsgsSharesFromProxies(msg);
			status.forEach(print.andThen(assertResult));
		});

		ThresholdProxySignature signature = signMembers.get(0).generateProxyThresholdSignature(msg);
		boolean valid = Verifier.verifyThresholdProxySignature(signature);
		assertThat("Threshold proxy unprotected signature is not valid", valid, is(true));
	}

	private List<ProxyThresholdSigner> generateGroupMembers(Group thresholdGroup, int size) {
		return IntStream.range(0, size)
				.mapToObj(i -> new ProxyThresholdSigner(DsaKeyManagement.generateWrappedKeyPair()))
				.peek(s -> s.setGroup(thresholdGroup))
				.collect(toList());
	}

}
