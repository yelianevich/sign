package by.sign.proxy;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.junit.Test;

import by.sign.proxy.artifact.Warrant;

public class WarrantTest {

	@Test
	public void shouldProduceParsableString() {
		DateTime now = DateTime.now();
		Period period = new Period().withDays(20).withHours(12);
		Warrant warrant = new Warrant(now, period);
		String parsableString = warrant.toString();
		warrant = Warrant.valueOf(parsableString);

		assertTrue(warrant.getValidFrom().isEqual(now));
		assertThat(warrant.getValidPeriod(), is(period));
	}
	
	@Test
	public void validIfNotExpired() {
		Warrant warrant = Warrant.getOneYearWarrant();

		assertTrue(warrant.isValid());
	}

	@Test
	public void notValidIfExpired() {
		DateTime now = DateTime.now().minusYears(1);
		Period period = new Period().withDays(20).withHours(12);
		Warrant warrant = new Warrant(now, period);

		assertFalse(warrant.isValid());
	}

}
