package by.sign.proxy.perf;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.Security;
import java.security.interfaces.DSAPrivateKey;
import java.security.interfaces.DSAPublicKey;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.stereotype.Component;

import by.sign.proxy.DsaSignatureProcessor;
import by.sign.proxy.actor.OriginalSigner;
import by.sign.proxy.actor.ProxySigner;
import by.sign.proxy.actor.Verifier;
import by.sign.proxy.artifact.Proxy;
import by.sign.proxy.artifact.SubstituteProxy;
import by.sign.proxy.artifact.Warrant;
import by.sign.proxy.key.DsaKeyManagement;
import by.sign.proxy.key.DsaKeyPairWrapper;

@Component
public class ProxyDsaVsPlainDsaPerformance {

	private static KeyPair keyPair;
	private static DsaSignatureProcessor dsaSigner;

	private static DsaKeyPairWrapper keyPairWrapper;
	private static OriginalSigner originalSigner;

	private static DsaKeyPairWrapper keyPairWrappedProxy;
	private static ProxySigner proxySigner;
	private static DsaSignatureProcessor unprotectedSigner;
	private static DsaSignatureProcessor protectedSigner;

	private Proxy proxy;

	private DSAPublicKey publicKey;
	private DSAPrivateKey privateKey;
	private byte[] content;

	@PostConstruct
	public void setUp() throws Exception {
		Security.addProvider(new BouncyCastleProvider());

		keyPair = DsaKeyManagement.generateKeyPair();
		dsaSigner = new DsaSignatureProcessor();

		keyPairWrapper = DsaKeyManagement.generateWrappedKeyPair();
		originalSigner = new OriginalSigner(keyPairWrapper);

		keyPairWrappedProxy = DsaKeyManagement.generateWrappedKeyPair();;
		proxySigner = new ProxySigner(keyPairWrappedProxy);
	}

	@PreDestroy
	public void tearDown() throws Exception {
		Security.removeProvider("BC");
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	//@Timed
	public void signAndVerifyDsa() {
		byte[] sign = dsaSigner.sign(keyPair.getPrivate(), content);
		boolean valid = dsaSigner.verify(keyPair.getPublic(), content, sign);
		assertThat(valid, is(true));
	}

	//@Timed
	public void computeUnprotectedKeys() {
		Warrant warrant = Warrant.getOneYearWarrant();
		proxy = originalSigner.generateProxy(warrant);
		BigInteger yp = Verifier.computeUnprotectedPublicKey(proxy);
		publicKey = DsaKeyManagement.getPublicKey(yp);
		privateKey = DsaKeyManagement.getUnprotectedPrivateKey(proxy);
		unprotectedSigner = new DsaSignatureProcessor();
	}

	//@Timed
	public void signAndVerifyUnprotectedProxy() {
		byte[] sign = unprotectedSigner.sign(privateKey, content);
		boolean valid = unprotectedSigner.verify(publicKey, content, sign);

		assertThat(valid, is(true));
	}

	//@Timed
	public void computeProtectedKeys() {
		Warrant warrant = Warrant.getOneYearWarrant();
		proxy = originalSigner.generateProxy(warrant);
		SubstituteProxy substituteProxy = proxySigner.generateSubstituteProxy(proxy);
		BigInteger yp = Verifier.computeProtectedPublicKey(substituteProxy);
		publicKey = DsaKeyManagement.getPublicKey(yp);
		privateKey = DsaKeyManagement.getProtectedPrivateKey(substituteProxy);
		protectedSigner = new DsaSignatureProcessor();
	}

	//@Timed
	public void signAndVerifyProtectedProxy() {
		byte[] sign = protectedSigner.sign(privateKey, content);
		boolean valid = protectedSigner.verify(publicKey, content, sign);
		assertThat(valid, is(true));
	}

}
