package by.sign.proxy.perf;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertThat;

import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.IntStream;

import org.springframework.stereotype.Component;

import com.codahale.metrics.annotation.Timed;

import by.sign.proxy.actor.OriginalSigner;
import by.sign.proxy.actor.ProxyThresholdSigner;
import by.sign.proxy.actor.Verifier;
import by.sign.proxy.actor.VerifyStatus;
import by.sign.proxy.artifact.Group;
import by.sign.proxy.artifact.ThresholdProxySignature;
import by.sign.proxy.artifact.Warrant;
import by.sign.proxy.key.DsaKeyManagement;
import by.sign.proxy.key.DsaKeyPairWrapper;

@Component
public class ThresholdPerformance {

	private OriginalSigner originalSigner;
	private DsaKeyPairWrapper keyPairWrapper;

	private Group group;
	private List<ProxyThresholdSigner> members;

	private byte[] msg;

	public void setContent(byte[] msg) {
		this.msg = msg;
	}

	public void initGroup(int groupSize, int threshold) {
		keyPairWrapper = DsaKeyManagement.generateWrappedKeyPair();
		originalSigner = new OriginalSigner(keyPairWrapper);

		group = new Group();
		members = generateGroupMembers(group, groupSize);
		group.setMembers(members);
		group.setSignPublishers(members.subList(0, threshold));
		group.setThreshold(threshold);
	}

	@Timed
	public void generateProxyShareAndVerify() {
		originalSigner.setProxySharesToGroup(group, Warrant.getOneYearWarrant());
		for (ProxyThresholdSigner proxySigner : members) {
			boolean isValidProxyShare = proxySigner.verifyProxyShare();
			assertThat("Proxy Share not valid", isValidProxyShare, is(true));
		}
	}

	@Timed
	public void shareAndVerifyGroupSecret() {
		List<ProxyThresholdSigner> signMembers = group.getSignPublishers();
		// 1. Random number generation in the group
		signMembers.forEach(ProxyThresholdSigner::shareSecretToGroup);
		List<Integer> notValidShare = signMembers.stream()
				.map(ProxyThresholdSigner::verifyGroupShares)
				.filter(list -> !list.isEmpty())
				.findFirst()
				.orElse(Collections.emptyList());
		assertThat("Random number generation failed for group", notValidShare, is(empty()));

		signMembers.forEach(ProxyThresholdSigner::computeGroupShares);
		signMembers.forEach(signer -> signer.signWithProxyShare(msg));
		Consumer<VerifyStatus> assertResult = r -> assertThat(r.isValid(), is(true));
		signMembers.forEach(s -> {
			List<VerifyStatus> status = s.verifySignedMsgsSharesFromProxies(msg);
			status.forEach(assertResult);
		});
	}

	@Timed
	public void thresholdGroupCanGenerateProxyUnprotectedSignature() {
		List<ProxyThresholdSigner> signMembers = group.getSignPublishers();

		ThresholdProxySignature signature = signMembers.get(0).generateProxyThresholdSignature(msg);
		boolean valid = Verifier.verifyThresholdProxySignature(signature);
		assertThat("Threshold proxy unprotected signature is not valid", valid, is(true));
	}

	private List<ProxyThresholdSigner> generateGroupMembers(Group thresholdGroup, int size) {
		return IntStream.range(0, size)
				.mapToObj(i -> new ProxyThresholdSigner(DsaKeyManagement.generateWrappedKeyPair()))
				.peek(s -> s.setGroup(thresholdGroup))
				.collect(toList());
	}

}
