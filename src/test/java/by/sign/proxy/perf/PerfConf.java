package by.sign.proxy.perf;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.ryantenney.metrics.spring.config.annotation.EnableMetrics;
import com.ryantenney.metrics.spring.config.annotation.MetricsConfigurerAdapter;

@Configuration
@ComponentScan("by.sign.proxy.perf")
@EnableMetrics
public class PerfConf extends MetricsConfigurerAdapter {

}
