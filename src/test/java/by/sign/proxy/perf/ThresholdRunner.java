package by.sign.proxy.perf;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.IntStream;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.CsvReporter;
import com.codahale.metrics.MetricRegistry;

public class ThresholdRunner {

	private static final Logger LOG = Logger.getLogger(Runner.class);
	private static final File RESULT_DIR = new File("./artifacts");
	private static final int T = 500;

	public static void main(String[] args) throws Exception {
		Path testFile = Paths.get("./artifacts/sample.docx");
		LOG.info("Test file: " + testFile.getFileName().toAbsolutePath());
		byte[] content = Files.readAllBytes(testFile);
		LOG.info("File size: " + content.length);

		try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(PerfConf.class)) {
			ThresholdPerformance perfTest = context.getBean(ThresholdPerformance.class);
			MetricRegistry metrics = context.getBean(MetricRegistry.class);

			int groupSize = 7;
			int threshold = 7;

			LOG.info("Group size: " + groupSize);
			LOG.info("Threshold: " + threshold);

			perfTest.setContent(content);
			perfTest.initGroup(groupSize, threshold);

			IntStream.range(0, T).forEach(i -> perfTest.generateProxyShareAndVerify());
			IntStream.range(0, T).forEach(i -> perfTest.shareAndVerifyGroupSecret());
			IntStream.range(0, T).forEach(i -> perfTest.thresholdGroupCanGenerateProxyUnprotectedSignature());

			CsvReporter reporter = CsvReporter.forRegistry(metrics).build(RESULT_DIR);
			ConsoleReporter console = ConsoleReporter.forRegistry(metrics).build();
			reporter.report();
			console.report();
		}
	}

}
