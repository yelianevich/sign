package by.sign.proxy;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

import java.math.BigInteger;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.sign.proxy.actor.ProxyHelper;
import by.sign.proxy.artifact.Warrant;
import by.sign.proxy.key.DsaKeyManagement;

public class ProxyHelperTest {

	@Before
	public void setUp() throws Exception {
		Security.addProvider(new BouncyCastleProvider());
	}

	@After
	public void tearDown() throws Exception {
		Security.removeProvider("BC");
	}

	@Test
	public void hashShouldReturnNotZeroValue() {
		Warrant warrant = Warrant.getOneYearWarrant();
		BigInteger K = new BigInteger(ParamsHolder.KEY_SPACE, DsaKeyManagement.RANDOM);
		BigInteger hash = ProxyHelper.hash(warrant, K);
		System.out.println(hash.toString(16));

		assertThat(hash, is(not(BigInteger.ZERO)));

	}

}
