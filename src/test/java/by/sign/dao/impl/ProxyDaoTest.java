package by.sign.dao.impl;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isIn;
import static org.junit.Assert.assertThat;

import java.security.Security;
import java.util.List;

import javax.annotation.Resource;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.sign.dao.IProxyDao;
import by.sign.enums.ProxyType;
import by.sign.proxy.actor.OriginalSigner;
import by.sign.proxy.actor.ProxySigner;
import by.sign.proxy.artifact.Proxy;
import by.sign.proxy.artifact.SubstituteProxy;
import by.sign.proxy.artifact.Warrant;
import by.sign.proxy.key.DsaKeyManagement;
import by.sign.proxy.key.DsaKeyPairWrapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/configuration/spring/beans/app_context.xml" })
public class ProxyDaoTest {
	private static Proxy proxy;
	private static SubstituteProxy subProxy;

	@Resource(name = "proxyDao")
	private IProxyDao<Proxy> proxyDao;

	@Resource(name = "proxyDao")
	private IProxyDao<SubstituteProxy> subProxyDao;

	@BeforeClass
	public static void setUp() throws Exception {
		Security.addProvider(new BouncyCastleProvider());

		DsaKeyPairWrapper keyPairWrapper = DsaKeyManagement.generateWrappedKeyPair();
		OriginalSigner originalSigner = new OriginalSigner(keyPairWrapper);
		Warrant warrant = Warrant.getOneYearWarrant();
		proxy = originalSigner.generateProxy(warrant);
		proxy.setTargetUser("ubsuir");
		DsaKeyPairWrapper keyPairWrappedProxy = DsaKeyManagement.generateWrappedKeyPair();
		ProxySigner proxySigner = new ProxySigner(keyPairWrappedProxy);

		subProxy = proxySigner.generateSubstituteProxy(proxy);
	}

	@AfterClass
	public static void tearDown() throws Exception {
		Security.removeProvider("BC");
	}

	@Test
	public void proxyDaoShouldAddAndRemoveProxy() {
		boolean added = proxyDao.addProxy(proxy, ProxyType.UNPROTECTED);
		assertThat(added, is(true));

		boolean deleted = proxyDao.deleteProxy(proxy.getProxyId());
		proxy.setProxyId(null);
		assertThat(deleted, is(true));
	}

	@Test
	public void proxyDaoShouldAddAndRemoveSubProxy() {
		boolean added = proxyDao.addProxy(subProxy, ProxyType.PROTECTED);
		assertThat(added, is(true));

		boolean deleted = proxyDao.deleteProxy(subProxy.getProxyId());
		subProxy.setProxyId(null);
		assertThat(deleted, is(true));
	}

	@Test
	public void proxyShoudBeAbleToGetProxy() throws InstantiationException, IllegalAccessException {
		boolean added = proxyDao.addProxy(subProxy, ProxyType.PROTECTED);
		assertThat(added, is(true));

		SubstituteProxy retrievedProxy = subProxyDao.getProxy(subProxy.getProxyId(), SubstituteProxy.class);
		assertThat(subProxy, is(retrievedProxy));
		
		boolean deleted = proxyDao.deleteProxy(subProxy.getProxyId());
		subProxy.setProxyId(null);
		assertThat(deleted, is(true));
	}

	@Test
	public void proxyShoudBeAbleToUpdateProxy() throws InstantiationException, IllegalAccessException {
		boolean added = proxyDao.addProxy(subProxy, ProxyType.PROTECTED);
		assertThat(added, is(true));

		String prevTargetUsername = subProxy.getTargetUser();
		subProxy.setTargetUser("ubsu");

		boolean updated = subProxyDao.updateProxy(subProxy);
		assertThat(updated, is(true));

		SubstituteProxy retrievedProxy = subProxyDao.getProxy(subProxy.getProxyId(), SubstituteProxy.class);
		assertThat(retrievedProxy.getTargetUser(), is(subProxy.getTargetUser()));
		subProxy.setTargetUser(prevTargetUsername);

		boolean deleted = proxyDao.deleteProxy(subProxy.getProxyId());
		subProxy.setProxyId(null);
		assertThat(deleted, is(true));
	}

	@Test
	public void shoudBeAbleToGetProxyList() throws InstantiationException, IllegalAccessException {
		boolean subProxyAdded = proxyDao.addProxy(subProxy, ProxyType.PROTECTED);
		assertThat(subProxyAdded, is(true));

		boolean proxyAdded = proxyDao.addProxy(proxy, ProxyType.UNPROTECTED);
		assertThat(proxyAdded, is(true));

		List<SubstituteProxy> subProxyList = subProxyDao.getProxyList(1, 10, SubstituteProxy.class,
				ProxyType.PROTECTED, "ubsuir");
		List<Proxy> proxyList = proxyDao.getProxyList(1, 10, Proxy.class, ProxyType.UNPROTECTED, "ubsuir");

		assertThat(subProxy, isIn(subProxyList));
		assertThat(proxy, isIn(proxyList));

		boolean subProxydeleted = proxyDao.deleteProxy(subProxy.getProxyId());
		subProxy.setProxyId(null);
		assertThat(subProxydeleted, is(true));

		boolean proxyDeleted = proxyDao.deleteProxy(proxy.getProxyId());
		proxy.setProxyId(null);
		assertThat(proxyDeleted, is(true));
	}

}
