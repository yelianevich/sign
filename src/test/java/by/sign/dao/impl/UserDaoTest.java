package by.sign.dao.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.sign.dao.IUserDao;
import by.sign.model.ServiceUser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/configuration/spring/beans/app_context.xml"})
public final class UserDaoTest {
	private IUserDao userDao;
	
	@Autowired
	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}
	
	@Before
	public void insertUser() {
		Set<GrantedAuthority> authorities = new HashSet<>();
		authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		ServiceUser serviceUser = new ServiceUser("username", "password", authorities);
		serviceUser.setEmail("email");
		serviceUser.setFirstName("fn");
		serviceUser.setLastName("ln");
		serviceUser.setPosition("position");
		serviceUser.setX(BigInteger.ONE);
		serviceUser.setY(BigInteger.TEN);
		userDao.saveUser(serviceUser);
	}

	@Test
	public void shouldReturnNotNullUser() {
		ServiceUser user = userDao.getUser("username");
		
		assertThat(user, is(notNullValue()));
	}
	
	@Test
	public void shouldReturnSameValues() {
		ServiceUser serviceUser = userDao.getUser("username");
		
		assertThat(serviceUser.getEmail(), is("email"));
		assertThat(serviceUser.getFirstName(), is("fn"));
		assertThat(serviceUser.getLastName(), is("ln"));
		assertThat(serviceUser.getPosition(), is("position"));
		assertThat(serviceUser.getX(), is(BigInteger.ONE));
		assertThat(serviceUser.getY(), is(BigInteger.TEN));
	}
	
	@After
	public void removeUser() {
		userDao.deleteUser("username");
	}
}