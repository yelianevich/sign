package by.sign.dao.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.sign.dao.ICompanyDao;
import by.sign.dao.IUserDao;
import by.sign.model.Company;
import by.sign.model.ServiceUser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/configuration/spring/beans/app_context.xml"})
public class CompanyDaoTest {
	private ICompanyDao companyDao;
	private IUserDao userDao;
	private Company company;
	
	@Autowired
	public void setCompanyDao(ICompanyDao companyDao) {
		this.companyDao = companyDao;
	}

	@Autowired
	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}

	@Before
	public void setUp() throws Exception {
		Set<GrantedAuthority> authorities = new HashSet<>();
		authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		ServiceUser serviceUser = new ServiceUser("username", "password", authorities);
		serviceUser.setEmail("email");
		serviceUser.setFirstName("fn");
		serviceUser.setLastName("ln");
		serviceUser.setPosition("position");
		userDao.deleteUser("username");
		userDao.saveUser(serviceUser);
		
		company = new Company();
		company.setIndustry("industry");
		company.setMaxUsers(5);
		company.setName("Name");
		List<ServiceUser> users = new ArrayList<>(1);
		users.add(serviceUser);
		company.setUsers(users);
	}

	@After
	public void tearDown() throws Exception {
		userDao.deleteUser("username");
	}

	@Test
	public void shouldDoCrudWithoutExceptions() {
		boolean inserted = companyDao.addCompany(company);
		assertThat(inserted, is(true));
		
		String id = company.getCompanyId();
		company = companyDao.getCompany(company.getCompanyId());
		
		assertThat(id, is(equalTo(company.getCompanyId())));
		
		company.setIndustry("Changed Industry");
		boolean updated = companyDao.updateCompany(company);
		
		assertThat(updated, is(true));
		
		company = companyDao.getCompany(company.getCompanyId());
		
		assertThat(company.getIndustry(), is(equalTo("Changed Industry")));
		assertThat(updated, is(true));
		
		companyDao.deleteCompany(company.getCompanyId());
		Company company = companyDao.getCompany(id);
		
		assertThat(company.getCompanyId(), isEmptyOrNullString());
	}

}
