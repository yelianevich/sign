package by.sign.model.mapper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import by.sign.model.ServiceUser;
import by.sign.proxy.artifact.Proxy;
import by.sign.proxy.artifact.SubstituteProxy;
import by.sign.proxy.artifact.Warrant;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class ModelMapperTest {
	private static ServiceUser serviceUser; 
	private ModelMapper<ServiceUser> userMapper = new ModelMapper<ServiceUser>();
	private static DBObject dbObj = new BasicDBObject();;
	
	@BeforeClass
	public static void createModel() {
		Set<GrantedAuthority> authorities = new HashSet<>();
		authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		serviceUser = new ServiceUser("username", "password", authorities);
		serviceUser.setEmail("email");
		serviceUser.setFirstName("fn");
		serviceUser.setLastName("ln");
		serviceUser.setPosition("position");
		serviceUser.setX(BigInteger.ONE);
		serviceUser.setY(BigInteger.TEN);
		
		dbObj.put("email", "email");
		dbObj.put("firstName", "fn");
		dbObj.put("lastName", "ln");
		dbObj.put("position", "position");
		dbObj.put("x", BigInteger.ONE.toString(16));
		dbObj.put("y", BigInteger.TEN.toString(16));
	}

	@Test(expected = IllegalArgumentException.class)
	public void toDbObjectThrowExceptionOnNullModel() {
		userMapper.toDbObject(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void restoreModelThrowExceptionOnNullDbObject() {
		userMapper.restoreModel(null, serviceUser);
	}

	@Test(expected = IllegalArgumentException.class)
	public void restoreModelThrowExceptionOnNullModel() {
		userMapper.restoreModel(dbObj, null);
	}

	@Test
	public void mapSimpleModel() {
		DBObject dbObject = userMapper.toDbObject(serviceUser);
		assertThat(dbObject.get("email").toString(), is("email"));
		assertThat(dbObject.get("firstName").toString(), is("fn"));
		assertThat(dbObject.get("lastName").toString(), is("ln"));
		assertThat(dbObject.get("position").toString(), is("position"));
		assertThat(dbObject.get("x").toString(), is(BigInteger.ONE.toString(16)));
		assertThat(dbObject.get("y").toString(), is(BigInteger.TEN.toString(16)));
	}
	
	@Test
	public void restoreSimpleModel() {
		serviceUser = userMapper.restoreModel(dbObj, serviceUser);
		assertThat(serviceUser.getEmail(), is("email"));
		assertThat(serviceUser.getFirstName(), is("fn"));
		assertThat(serviceUser.getLastName(), is("ln"));
		assertThat(serviceUser.getPosition(), is("position"));
		assertThat(serviceUser.getX(), is(BigInteger.ONE));
		assertThat(serviceUser.getY(), is(BigInteger.TEN));
	}

	@Test
	public void mapAndResotreModelWithInheritance() {
		BigInteger sigm = BigInteger.ZERO;
		BigInteger K = BigInteger.ONE;
		BigInteger originalY = BigInteger.TEN;
		Warrant warrant = Warrant.getOneYearWarrant();
		Proxy proxy = new Proxy(warrant, sigm, K, originalY);
		BigInteger sigmSubstitute = new BigInteger("11");
		BigInteger proxyY = new BigInteger("12");
		SubstituteProxy substituteProxy = new SubstituteProxy(proxy, sigmSubstitute, proxyY);
		ModelMapper<SubstituteProxy> mapper = new ModelMapper<>();

		DBObject dbObj = mapper.toDbObject(substituteProxy);
		assertEquals(dbObj.get("sigm"), sigm.toString(16));
		assertEquals(dbObj.get("K"), K.toString(16));
		assertEquals(dbObj.get("originalY"), originalY.toString(16));
		assertEquals(dbObj.get("sigmSubstitute"), sigmSubstitute.toString(16));
		assertEquals(dbObj.get("proxyY"), proxyY.toString(16));
		
		SubstituteProxy emptySubstituteProxy = new SubstituteProxy();
		SubstituteProxy restoredSubstituteProxy = mapper.restoreModel(dbObj, emptySubstituteProxy);

		assertEquals(substituteProxy, restoredSubstituteProxy);

		ModelMapper<Proxy> proxyMapper = new ModelMapper<>();
		DBObject dbo = proxyMapper.toDbObject(substituteProxy);
		Proxy proxyEmpty = new Proxy();
		proxyMapper.restoreModel(dbo, proxyEmpty);

		System.out.println(dbo);
	}

}
