package by.sign.proxy.key;

import static by.sign.proxy.ParamsHolder.G;
import static by.sign.proxy.ParamsHolder.P;
import static by.sign.proxy.ParamsHolder.Q;

import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.interfaces.DSAPrivateKey;
import java.security.interfaces.DSAPublicKey;
import java.security.spec.DSAParameterSpec;
import java.security.spec.DSAPrivateKeySpec;
import java.security.spec.DSAPublicKeySpec;
import java.security.spec.InvalidKeySpecException;

import org.apache.log4j.Logger;

import by.sign.model.ServiceUser;
import by.sign.proxy.artifact.Proxy;
import by.sign.proxy.artifact.SubstituteProxy;
import by.sign.util.ContextListener;

public final class DsaKeyManagement {
	private static final Logger LOG = Logger.getLogger(ContextListener.class);
	public static final SecureRandom RANDOM = new SecureRandom();

	private DsaKeyManagement() {
	}

	public static KeyPair generateKeyPair() {
		KeyPair keyPair = null;
		KeyPairGenerator keyGenerator;
		try {
			keyGenerator = KeyPairGenerator.getInstance("DSA", "BC");
			DSAParameterSpec dsaSpec = new DSAParameterSpec(P, Q, G);
			keyGenerator.initialize(dsaSpec, RANDOM);
			keyPair = keyGenerator.generateKeyPair();
		} catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidAlgorithmParameterException e) {
			LOG.error("Key generation failed", e);
		}
		return keyPair;
	}

	public static DsaKeyPairWrapper getKeyPair(ServiceUser user) {
		DSAPrivateKey privateKey = generatePrivateKey(user.getX());
		DSAPublicKey publicKey = getPublicKey(user.getY());
		KeyPair keyPair = new KeyPair(publicKey, privateKey);
		DsaKeyPairWrapper keyPairWrapper = new DsaKeyPairWrapper(keyPair);
		return keyPairWrapper;
	}

	public static DsaKeyPairWrapper generateWrappedKeyPair() {
		KeyPair keyPair = generateKeyPair();
		return new DsaKeyPairWrapper(keyPair);
	}

	public static DSAPrivateKey getUnprotectedPrivateKey(Proxy proxy) {
		return generatePrivateKey(proxy.getSigm());
	}

	public static DSAPrivateKey getPrivateKey(ServiceUser user) {
		return generatePrivateKey(user.getX());
	}

	public static DSAPrivateKey getProtectedPrivateKey(SubstituteProxy proxy) {
		return generatePrivateKey(proxy.getSigmSubstitute());
	}

	public static DSAPrivateKey generatePrivateKey(BigInteger x) {
		DSAPrivateKey privateKey = null;
		DSAPrivateKeySpec privateKeySpec = new DSAPrivateKeySpec(x, P, Q, G);
		try {
			KeyFactory keyFactory = KeyFactory.getInstance("DSA", "BC");
			privateKey = (DSAPrivateKey) keyFactory.generatePrivate(privateKeySpec);
		} catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException e) {
			LOG.error("Cannot generate private key", e);
		}
		return privateKey;
	}
	
	public static DSAPublicKey getPublicKey(BigInteger y) {
		DSAPublicKey publicKey = null;
		DSAPublicKeySpec publicKeySpec = new DSAPublicKeySpec(y, P, Q, G);
		try {
			KeyFactory keyFactory = KeyFactory.getInstance("DSA", "BC");
			publicKey = (DSAPublicKey) keyFactory.generatePublic(publicKeySpec);
		} catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException e) {
			LOG.error("Cannot generate public key", e);
		}
		return publicKey;
	}

}
