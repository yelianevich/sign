package by.sign.proxy.key;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.DSAPrivateKey;
import java.security.interfaces.DSAPublicKey;

/**
 * Wrapper for DSA key pair to get quick access to underlying numbers
 * 
 * @author Raman Yelianevich
 */
public class DsaKeyPairWrapper {
	private KeyPair keyPair;
	private BigInteger y;
	private BigInteger x;

	public DsaKeyPairWrapper(KeyPair keyPair) {
		this.keyPair = keyPair;
		DSAPrivateKey dsaSk = (DSAPrivateKey) keyPair.getPrivate();
		x = dsaSk.getX();
		DSAPublicKey dsaPk = (DSAPublicKey) keyPair.getPublic();
		y = dsaPk.getY();
	}

	public PublicKey getPublic() {
		return keyPair.getPublic();
	}

	public PrivateKey getPrivate() {
		return keyPair.getPrivate();
	}

	public int hashCode() {
		return keyPair.hashCode();
	}

	public boolean equals(Object obj) {
		return keyPair.equals(obj);
	}

	public String toString() {
		return keyPair.toString();
	}

	/**
	 * @return public key number of wrapped KeyPair
	 */
	public BigInteger getY() {
		return y;
	}

	/**
	 * @return private key number of wrapped KeyPair
	 */
	public BigInteger getX() {
		return x;
	}

	public DSAPublicKey getPublicKey() {
		return (DSAPublicKey) keyPair.getPublic();
	}

}
