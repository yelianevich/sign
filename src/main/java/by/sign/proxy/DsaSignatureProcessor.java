package by.sign.proxy;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;

import org.apache.log4j.Logger;

/**
 * Sign content and verify signatures using DSA algorithm
 * 
 * @author Raman Yelianevich
 * 
 */
public class DsaSignatureProcessor {
	private static final Logger LOG = Logger.getLogger(DsaSignatureProcessor.class);
	private Signature signature;

	public DsaSignatureProcessor() {
		try {
			signature = Signature.getInstance("SHA512withDSA", "BC");
		} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
			LOG.error("DsaSignatureProcessor creation failed", e);
		}
	}

	public byte[] sign(PrivateKey privateKey, byte[] content) {
		byte[] sign = null;
		try {
			signature.initSign(privateKey);
			signature.update(content);
			sign = signature.sign();
		} catch (SignatureException | InvalidKeyException e) {
			LOG.error("Signing operation failed", e);
		}
		return sign;
	}

	public boolean verify(PublicKey publicKey, byte[] content, byte[] sign) {
		try {
			signature.initVerify(publicKey);
			signature.update(content);
			return signature.verify(sign);
		} catch (InvalidKeyException | SignatureException e) {
			LOG.info("Cannot verify signature", e);
		}
		return false;
	}

}
