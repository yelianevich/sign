package by.sign.proxy.artifact;

import java.math.BigInteger;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import by.sign.model.mapper.MapperModel;
import by.sign.model.mapper.Prop;

/**
 * @author Raman Yelianevich
 */
@MapperModel
public class Proxy {

	@Prop("_id")
	private String proxyId;

	@Prop
	private Warrant warrant;

	@Prop
	private BigInteger sigm;

	@Prop
	private BigInteger K;

	@Prop
	private BigInteger originalY;

	@Prop
	private String targetUser;

	public Proxy() {
	}

	public Proxy(Warrant warrant, BigInteger sigm, BigInteger K, BigInteger originalY) {
		this.warrant = warrant;
		this.sigm = sigm;
		this.K = K;
		this.originalY = originalY;
	}

	public void setWarrant(Warrant warrant) {
		this.warrant = warrant;
	}

	public Warrant getWarrant() {
		return warrant;
	}

	public void setSigm(BigInteger sigm) {
		this.sigm = sigm;
	}

	public BigInteger getSigm() {
		return sigm;
	}

	public BigInteger getK() {
		return K;
	}

	public void setK(BigInteger k) {
		K = k;
	}

	public BigInteger getOriginalY() {
		return originalY;
	}

	public void setOriginalY(BigInteger originalY) {
		this.originalY = originalY;
	}

	public String getProxyId() {
		return proxyId;
	}

	public void setProxyId(String proxyId) {
		this.proxyId = proxyId;
	}

	public String getTargetUser() {
		return targetUser;
	}

	public void setTargetUser(String targetUser) {
		this.targetUser = targetUser;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
