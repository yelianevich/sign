package by.sign.proxy.artifact;

import java.math.BigInteger;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import by.sign.model.mapper.MapperModel;
import by.sign.model.mapper.Prop;

@MapperModel
public class SubstituteProxy extends Proxy {

	@Prop
	private BigInteger sigmSubstitute;

	@Prop
	private BigInteger proxyY;

	public SubstituteProxy() {
	}

	public SubstituteProxy(Proxy p, BigInteger sigmSubstitute, BigInteger proxyY) {
		super(p.getWarrant(), p.getSigm(), p.getK(), p.getOriginalY());
		this.sigmSubstitute = sigmSubstitute;
		this.proxyY = proxyY;
		setTargetUser(p.getTargetUser());
	}

	public BigInteger getProxyY() {
		return proxyY;
	}

	public void setProxyY(BigInteger proxyY) {
		this.proxyY = proxyY;
	}

	public BigInteger getSigmSubstitute() {
		return sigmSubstitute;
	}

	public void setSigmSubstitute(BigInteger sigmSubstitute) {
		this.sigmSubstitute = sigmSubstitute;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return "SubstituteProxy [proxyY=" + proxyY + "]";
	}

}
