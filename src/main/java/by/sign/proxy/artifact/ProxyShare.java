package by.sign.proxy.artifact;

import java.math.BigInteger;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ProxyShare {

	private Group group;
	private Proxy proxy;
	private BigInteger proxyShare;
	private int proxyShareIndex;

	public ProxyShare() {
	}

	public ProxyShare(Group group, Proxy proxy, BigInteger proxyShare, int proxyShareIndex) {
		this.proxy = proxy;
		this.proxyShare = proxyShare;
		this.group = group;
		this.proxyShareIndex = proxyShareIndex;
	}

	public BigInteger getProxyShare() {
		return proxyShare;
	}

	public void setProxyShare(BigInteger proxyShare) {
		this.proxyShare = proxyShare;
	}

	public Proxy getProxy() {
		return proxy;
	}

	public void setProxy(Proxy proxy) {
		this.proxy = proxy;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public int getProxyShareIndex() {
		return proxyShareIndex;
	}

	public void setProxyShareIndex(int proxyShareIndex) {
		this.proxyShareIndex = proxyShareIndex;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
