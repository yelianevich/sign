package by.sign.proxy.artifact;

import java.math.BigInteger;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import by.sign.proxy.actor.ProxyThresholdSigner;

public class Group {

	private int size;
	private int threshold;
	private BigInteger[] proxySharePublic;
	private List<BigInteger> groupSharePublic;
	private List<ProxyThresholdSigner> members;
	private List<ProxyThresholdSigner> signPublishers;

	public Group() {
	}

	public Group(int size, int threshold) {
		if (threshold > size) {
			throw new IllegalArgumentException("Threshold value cannot be less than group size");
		}
		this.size = size;
		this.threshold = threshold;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getThreshold() {
		return threshold;
	}

	public void setThreshold(int threshold) {
		this.threshold = threshold;
	}

	public BigInteger[] getProxySharePublic() {
		return proxySharePublic;
	}

	public void setProxySharePublic(BigInteger[] proxySharePublic) {
		this.proxySharePublic = proxySharePublic;
	}

	public List<BigInteger> getGroupSharePublic() {
		return groupSharePublic;
	}

	public void setGroupSharePublic(List<BigInteger> groupSharePublic) {
		this.groupSharePublic = groupSharePublic;
	}

	public List<ProxyThresholdSigner> getMembers() {
		return members;
	}

	public void setMembers(List<ProxyThresholdSigner> members) {
		this.members = members;
		this.size = members.size();
	}

	public List<ProxyThresholdSigner> getSignPublishers() {
		return signPublishers;
	}

	public void setSignPublishers(List<ProxyThresholdSigner> signPublishers) {
		this.signPublishers = signPublishers;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
