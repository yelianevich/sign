package by.sign.proxy.artifact;

import java.math.BigInteger;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ThresholdProxySignature {
	private Group group;
	private byte[] message;
	private BigInteger t;
	private BigInteger j;
	private Proxy proxy;
	
	public ThresholdProxySignature() {
	}

	public ThresholdProxySignature(Group group, byte[] message, BigInteger t, BigInteger j, Proxy proxy) {
		this.group = group;
		this.message = message;
		this.t = t;
		this.j = j;
		this.proxy = proxy;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public byte[] getMessage() {
		return message;
	}

	public void setMessage(byte[] message) {
		this.message = message;
	}

	public BigInteger getT() {
		return t;
	}

	public void setT(BigInteger t) {
		this.t = t;
	}

	public BigInteger getJ() {
		return j;
	}

	public void setJ(BigInteger j) {
		this.j = j;
	}

	public Proxy getProxy() {
		return proxy;
	}

	public void setProxy(Proxy proxy) {
		this.proxy = proxy;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
