package by.sign.proxy.artifact;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.joda.time.DateTime;
import org.joda.time.Period;

/**
 * Immutable warrant implementation with valid period
 * 
 * @author Raman Yelianevich
 */
public final class Warrant {

	private DateTime validFrom;
	private Period validPeriod;

	public Warrant(DateTime validFrom, Period validPeriod) {
		this.validFrom = validFrom;
		this.validPeriod = validPeriod;
	}

	public boolean isValid() {
		boolean isNotFuture = !validFrom.isAfterNow();
		boolean inDateRange = validFrom.plus(validPeriod).isAfterNow();
		return isNotFuture && inDateRange;
	}

	public static Warrant valueOf(String warrantStr) {
		if (warrantStr == null) {
			throw new IllegalArgumentException("Warrant should be not null");
		}

		String[] fromPeriod = warrantStr.split(";");
		String from = fromPeriod[0];
		String period = fromPeriod[1];

		DateTime validFrom = DateTime.parse(from);
		Period validPeriod = Period.parse(period);

		return new Warrant(validFrom, validPeriod);
	}

	public static Warrant getOneYearWarrant() {
		DateTime now = DateTime.now();
		Period period = new Period().withYears(1);
		return new Warrant(now, period);
	}

	public String getLabel() {
		return "From " + validFrom.toString("dd/MM/yy") + " for " + validPeriod.getDays() + " days";
	}

	public DateTime getValidFrom() {
		return validFrom;
	}

	public Period getValidPeriod() {
		return validPeriod;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	/**
	 * Produce parsable string
	 */
	@Override
	public String toString() {
		return validFrom.toString() + ";" + validPeriod.toString();
	}

}
