package by.sign.proxy;

import java.math.BigInteger;
import java.nio.charset.Charset;

public final class ParamsHolder {

	private ParamsHolder() {
	}

	public static final int KEY_RADIX = 16;
	public static final Charset SIGN_TYPE_CHARSET = Charset.forName("UTF-16");
	public static final BigInteger G = new BigInteger(
			"185f8edebb68bed154d18b5199624e885ebde4b62e64cc0bfeaee7d9980567a34469e84ae534aed90db6d0afec785d162843e64d4ed1de2735acf28bb2cd39896e593598b0542e5568e1d456b3dfa1fd500dd265102b548281e01263b5eb7be1470d76715698f631212c09c51a6f68fc14232937b954cc760f30b55ce65072d6",
			16);
	public static final BigInteger P = new BigInteger(
			"bbf420793ec55edde372adf3089e46d7e79f3dc8476824714143a47a717754350a30b6cf84f8f01baea48bad217abe2817a6f36ccf32b3700f4b25460cf4426ad48de093bdc48672a3dedd13c1d270e18345f9c2fded7944ec14fad4b16f38439b5d537c2e5251b09f46c5fa8de9da644421e599f72e513d634d360c65577731",
			16);
	public static final BigInteger P_MINUS_ONE = P.subtract(BigInteger.ONE);
	public static final BigInteger Q = new BigInteger("cc3ab478d13dae03ef428d465009707b76d295a3", 16);
	public static final int KEY_SPACE = 1024;

}
