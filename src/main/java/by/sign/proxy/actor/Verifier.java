package by.sign.proxy.actor;

import static by.sign.proxy.ParamsHolder.G;
import static by.sign.proxy.ParamsHolder.KEY_RADIX;
import static by.sign.proxy.ParamsHolder.P;
import static by.sign.proxy.ParamsHolder.SIGN_TYPE_CHARSET;

import java.math.BigInteger;

import org.bouncycastle.util.Arrays;

import by.sign.proxy.artifact.Proxy;
import by.sign.proxy.artifact.SubstituteProxy;
import by.sign.proxy.artifact.ThresholdProxySignature;
import by.sign.proxy.artifact.Warrant;

public class Verifier {

	private Verifier() {
	}

	public static BigInteger computeUnprotectedPublicKey(Proxy proxy) {
		Warrant warrant = proxy.getWarrant();
		BigInteger K = proxy.getK();
		BigInteger j = ProxyHelper.hash(warrant, K);
		BigInteger ya = proxy.getOriginalY();

		BigInteger yp = ya.modPow(j, P).multiply(K).mod(P);
		return yp;
	}

	public static BigInteger computeProtectedPublicKey(SubstituteProxy proxy) {
		Warrant warrant = proxy.getWarrant();
		BigInteger K = proxy.getK();
		BigInteger j = ProxyHelper.hash(warrant, K);
		BigInteger ya = proxy.getOriginalY();
		BigInteger yb = proxy.getProxyY();

		// use modulo multiplication lemma
		BigInteger yaJ = ya.modPow(j, P);
		BigInteger ybJ = yb.modPow(j, P);

		// K is already computed by modulo P, so we can use K without getting K mod P
		BigInteger yp = yaJ.multiply(ybJ).multiply(K).mod(P);
		return yp;
	}

	public static boolean verifyThresholdProxySignature(ThresholdProxySignature sign) {
		BigInteger gt = G.modPow(sign.getT(), P);

		Proxy proxy = sign.getProxy();
		BigInteger t1 = proxy.getK();
		BigInteger h = ProxyHelper.hash(proxy.getWarrant(), t1);
		BigInteger ya = proxy.getOriginalY();
		BigInteger j = sign.getJ();
		BigInteger inBrackets = ya.modPow(h, P).multiply(t1).mod(P).modPow(j.negate(), P);

		BigInteger d = gt.multiply(inBrackets).mod(P);
		byte[] dBytes = d.toString(KEY_RADIX).getBytes(SIGN_TYPE_CHARSET);
		byte[] dAndM = Arrays.concatenate(dBytes, sign.getMessage());
		BigInteger jComputed = ProxyHelper.hash(dAndM);
		return j.equals(jComputed);
	}

}
