package by.sign.proxy.actor;

import static by.sign.proxy.ParamsHolder.G;
import static by.sign.proxy.ParamsHolder.KEY_RADIX;
import static by.sign.proxy.ParamsHolder.KEY_SPACE;
import static by.sign.proxy.ParamsHolder.P;
import static by.sign.proxy.ParamsHolder.Q;
import static by.sign.proxy.ParamsHolder.SIGN_TYPE_CHARSET;
import static by.sign.proxy.actor.ProxyHelper.modMul;
import static by.sign.proxy.key.DsaKeyManagement.RANDOM;
import static java.math.BigInteger.ONE;
import static java.math.BigInteger.ZERO;
import static java.util.stream.Collectors.toList;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.BinaryOperator;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import by.sign.proxy.artifact.Group;
import by.sign.proxy.artifact.Proxy;
import by.sign.proxy.artifact.ProxyShare;
import by.sign.proxy.artifact.ThresholdProxySignature;
import by.sign.proxy.artifact.Warrant;
import by.sign.proxy.key.DsaKeyPairWrapper;

public class ProxyThresholdSigner extends ProxySigner {
	private Group group;
	private ProxyShare proxyShare;

	private Map<Integer, List<BigInteger>> publicKeyShares = new HashMap<>();
	private Map<Integer, BigInteger> privateKeyShares = new HashMap<>();

	private BigInteger u; // group private key
	private List<BigInteger> dL; // group public key
	private byte[] dBytes; // bytes of the first dL

	private Map<Integer, BigInteger> proxySignedMsg = new HashMap<>();

	public ProxyThresholdSigner(DsaKeyPairWrapper keyPairWrapper) {
		super(keyPairWrapper);
	}

	/**
	 * g ^ bi == (Ya ^ j * K) * loop MUL[j=1 -- t -1](Bj ^ (i ^ j)) mod p
	 * 
	 * due to multiplication lemma we can perform all caclulations by module
	 * (a∗b) mod m = [(a mod m)∗(b mod m)] mod m
	 */
	public boolean verifyProxyShare() {
		ProxyShare proxyShare = getProxyShare();
		Proxy proxy = proxyShare.getProxy();

		Warrant warrant = proxy.getWarrant();
		BigInteger K = proxy.getK();
		BigInteger j = ProxyHelper.hash(warrant, K);

		// g ^ bi mod P
		BigInteger bi = proxyShare.getProxyShare();
		BigInteger leftSide = G.modPow(bi, P);

		BigInteger rightSide = computePublicProxyShare(getIndexInGroup(), proxy, K, j);

		return leftSide.equals(rightSide);
	}

	private BigInteger computePublicProxyShare(int signerIndex, Proxy proxy, BigInteger K, BigInteger j) {
		// right1 = [Ya ^ j mod P] * [K mod P] mod P
		// K = g * k mod P, so it's not neccessary to do it again
		BigInteger right1 = proxy.getOriginalY().modPow(j, P).multiply(K).mod(P);

		BigInteger[] Bj = getGroup().getProxySharePublic();
		BigInteger i = BigInteger.valueOf(signerIndex);
		BigInteger rightSide = right1;
		// loop MUL[j=1 -- t-1]
		for (int ti = 0; ti < Bj.length; ++ti) {
			// Bj ^ (i ^ j) mod p
			BigInteger BjExp = i.pow(ti + 1);
			BigInteger bj = Bj[ti];
			rightSide = rightSide.multiply(bj.modPow(BjExp, P)).mod(P);
		}
		return rightSide;
	}

	public void shareSecretToGroup() {
		List<ProxyThresholdSigner> signPublishers = group.getSignPublishers();

		BigInteger secret = new BigInteger(KEY_SPACE, RANDOM).mod(Q);
		BigInteger[] coeffs = ProxyHelper.randomCoefficients(secret, signPublishers.size());
		List<BigInteger> publicCoeffs = Arrays.stream(coeffs)
				.map(coeff -> G.modPow(coeff, P))
				.collect(toList());

		IntFunction<BigInteger> polinom = ProxyHelper.polinomial(coeffs);
		signPublishers.forEach(signer -> {
			BigInteger secretShare = polinom.apply(signer.getIndexInGroup()).mod(Q);
			signer.securelyTrasferPrivateShare(getIndexInGroup(), secretShare);
			signer.setPublicKeyShares(getIndexInGroup(), publicCoeffs);
		});
	}

	/**
	 * Verify shares from members that about to sign a message
	 * 
	 * @return list of signer id (index in the group),
	 *         that computed not valid group share
	 */
	public List<Integer> verifyGroupShares() {
		return privateKeyShares.entrySet().stream()
			.filter(signerShare -> {
				Integer signer = signerShare.getKey();
				BigInteger share = signerShare.getValue();
				List<BigInteger> publicValues = publicKeyShares.get(signer);
				return !verifySecretFromSigner(signer, share, publicValues);
			})
			.map(Entry::getKey)
			.collect(Collectors.toList());
	}

	private boolean verifySecretFromSigner(Integer signer, BigInteger privateShare, List<BigInteger> publicValues) {
		BigInteger left = G.modPow(privateShare, P);
		if (publicValues == null) {
			return false;
		}
		BigInteger selfIndex = BigInteger.valueOf(getIndexInGroup());
		BigInteger right = IntStream.range(0, publicValues.size())
				.mapToObj(i -> {
					BigInteger publicValue = publicValues.get(i);
					BigInteger exp = selfIndex.pow(i);
					return publicValue.modPow(exp, P);
				})
				.reduce(BigInteger.ONE, modMul(P));
		return left.equals(right);
	}

	public void computeGroupShares() {
		u = privateKeyShares.values().stream()
			.reduce(BigInteger.ZERO, BigInteger::add)
			.mod(Q);

		BinaryOperator<List<BigInteger>> multiplyLists = (res, bi) -> {
			return IntStream.range(0, res.size())
				.mapToObj(i -> {
						BigInteger acc = res.get(i);
						BigInteger x = bi.get(i);
						return acc.multiply(x).mod(P);
					})
				.collect(Collectors.toList());
		};

		dL = publicKeyShares.values().stream()
				.reduce(multiplyLists)
				.orElseGet(Collections::emptyList);

		dBytes = dL.get(0).toString(KEY_RADIX).getBytes(SIGN_TYPE_CHARSET);
		group.setGroupSharePublic(dL);
	}

	public void signWithProxyShare(byte[] msg) {
		int myIndex = getIndexInGroup();
		byte[] dAndM = org.bouncycastle.util.Arrays.concatenate(dBytes, msg);
		BigInteger j = ProxyHelper.hash(dAndM);
		BigInteger bi = proxyShare.getProxyShare();
		BigInteger bMulJ = bi.multiply(j);
		BigInteger di = u.add(bMulJ).mod(Q);
		group.getSignPublishers().forEach(s -> s.transferProxySignedMsg(myIndex, di));
	}

	public List<VerifyStatus> verifySignedMsgsSharesFromProxies(byte[] content) {
		return group.getSignPublishers().stream()
			.map(signer -> {
					int L = signer.getIndexInGroup();
					List<BigInteger> lShares = dL; //publicKeyShares.get(L);

					// 0. left = g^dl
					BigInteger dl = proxySignedMsg.get(L);
					BigInteger left = G.modPow(dl, P);

					// compute right
					// 1. compute g^ui
					BigInteger gUi = IntStream.range(0, lShares.size())
						.mapToObj(j -> {
								BigInteger lPowJ = BigInteger.valueOf(L).pow(j);
								return lShares.get(j).modPow(lPowJ, P);
							})
						.reduce(ONE, modMul(P));

					// 2. compute g^bl
					ProxyShare proxyShare = getProxyShare();
					Proxy proxy = proxyShare.getProxy();
					Warrant warrant = proxy.getWarrant();
					BigInteger K = proxy.getK();
					BigInteger j = ProxyHelper.hash(warrant, K);
					BigInteger gBl = computePublicProxyShare(L, proxy, K, j);

					// 3. (g^bl) ^ h(dL, mp)
					byte[] dAndM = org.bouncycastle.util.Arrays.concatenate(dBytes, content);
					BigInteger h = ProxyHelper.hash(dAndM);
					BigInteger gBlPowH = gBl.modPow(h, P);

					// 4. right = g^ui * (g^bl) ^ h(dL, mp) mod p
					BigInteger right = gUi.multiply(gBlPowH).mod(P);
					boolean isShareSignValid = left.equals(right);
					return new VerifyStatus(isShareSignValid, L);
				})
			.collect(Collectors.toList());
	}

	public ThresholdProxySignature generateProxyThresholdSignature(byte[] content) {
		int L = group.getSignPublishers().size();

		// compute group signature value t using Lagrange Equation to di
		BigInteger t = proxySignedMsg.entrySet().stream()
				.map(iDi -> {
						int i = iDi.getKey();
						int[] js = IntStream.rangeClosed(1, L).filter(j -> j != i).toArray();
						int numerator = Arrays.stream(js).reduce(1, (acc, j) -> acc * -j);
						int denominator = Arrays.stream(js).reduce(1, (acc, j) -> acc * (i - j));

						BigInteger di = iDi.getValue();
						di = di.multiply(BigInteger.valueOf(numerator));
						return di.divide(BigInteger.valueOf(denominator));
					})
				.reduce(ZERO, BigInteger::add)
				.mod(Q);
		byte[] dAndM = org.bouncycastle.util.Arrays.concatenate(dBytes, content);
		BigInteger j = ProxyHelper.hash(dAndM);
		return new ThresholdProxySignature(group, content, t, j, getProxyShare().getProxy());
	}

	public void transferProxySignedMsg(int signerIndex, BigInteger proxyGroupKeyShare) {
		proxySignedMsg.put(signerIndex, proxyGroupKeyShare);
	}

	public void setPublicKeyShares(int signerIndex, List<BigInteger> di) {
		publicKeyShares.put(signerIndex, di);
	}

	public void securelyTrasferPrivateShare(int senderIndex, BigInteger privateShare) {
		privateKeyShares.put(senderIndex, privateShare);
	}

	public int getIndexInGroup() {
		return getProxyShare().getProxyShareIndex();
	}

	public ProxyShare getProxyShare() {
		return proxyShare;
	}

	public void setProxyShare(ProxyShare proxyShare) {
		this.proxyShare = proxyShare;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

}
