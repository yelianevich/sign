package by.sign.proxy.actor;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class VerifyStatus {

	private boolean valid;
	private int signerIndex;

	public VerifyStatus(boolean valid, int signerIndex) {
		this.valid = valid;
		this.signerIndex = signerIndex;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public int getSignerIndex() {
		return signerIndex;
	}

	public void setSignerIndex(int signerIndex) {
		this.signerIndex = signerIndex;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
