package by.sign.proxy.actor;

import static by.sign.proxy.ParamsHolder.G;
import static by.sign.proxy.ParamsHolder.P;
import static by.sign.proxy.ParamsHolder.P_MINUS_ONE;

import java.math.BigInteger;

import by.sign.proxy.artifact.Proxy;
import by.sign.proxy.artifact.SubstituteProxy;
import by.sign.proxy.artifact.Warrant;
import by.sign.proxy.key.DsaKeyPairWrapper;

public class ProxySigner {

	private DsaKeyPairWrapper keyPairWrapper;

	public ProxySigner(DsaKeyPairWrapper keyPairWrapper) {
		this.keyPairWrapper = keyPairWrapper;
	}

	public boolean verifyProxy(Proxy proxy) {
		// g^s = ya^j * K mod P
		BigInteger K = proxy.getK();
		Warrant warrant = proxy.getWarrant();
		BigInteger j = ProxyHelper.hash(warrant, K);

		BigInteger leftSide = G.modPow(proxy.getSigm(), P);

		// to find right side of equation, module multiplication lemma
		// could be used to simplify computation
		BigInteger op1 = proxy.getOriginalY().modPow(j, P);
		BigInteger rightSide = op1.multiply(K).mod(P);

		boolean equationIsValid = leftSide.equals(rightSide);
		return equationIsValid;
	}

	public SubstituteProxy generateSubstituteProxy(Proxy proxy) {
		// sp = s + sb * h(mw, K) mod p - 1;
		Warrant warrant = proxy.getWarrant();
		BigInteger K = proxy.getK();
		BigInteger sigm = proxy.getSigm();
		BigInteger x = keyPairWrapper.getX();

		BigInteger j = ProxyHelper.hash(warrant, K);
		BigInteger jModP = j.mod(P);
		BigInteger xJModP = x.multiply(jModP).mod(P);

		BigInteger sigmSubstitute = sigm.add(xJModP).mod(P_MINUS_ONE);
		SubstituteProxy substituteProxy = new SubstituteProxy(proxy, sigmSubstitute, keyPairWrapper.getY());
		return substituteProxy;
	}

}
