package by.sign.proxy.actor;

import static by.sign.proxy.ParamsHolder.KEY_SPACE;
import static by.sign.proxy.ParamsHolder.Q;
import static by.sign.proxy.ParamsHolder.SIGN_TYPE_CHARSET;
import static by.sign.proxy.key.DsaKeyManagement.RANDOM;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.function.BinaryOperator;
import java.util.function.IntFunction;

import org.apache.log4j.Logger;

import by.sign.proxy.artifact.Warrant;

public final class ProxyHelper {
	private static final Logger LOG = Logger.getLogger(ProxyHelper.class);

	private ProxyHelper() {
	}

	public static BigInteger hash(Warrant warrant, BigInteger K) {
		String warrantWithKey = warrant.toString() + ";" + K.toString(16);
		byte[] content = warrantWithKey.getBytes(SIGN_TYPE_CHARSET);
		return hash(content);
	}

	public static BigInteger hash(byte[] content) {
		MessageDigest h;
		BigInteger j = BigInteger.ZERO;
		try {
			h = MessageDigest.getInstance("SHA-256", "BC");
			byte[] warrantKeyDigest = h.digest(content);
			j = new BigInteger(1, warrantKeyDigest);
		} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
			LOG.error("Hash procedure fails", e);
		}
		return j;
	}

	public static BigInteger[] randomCoefficients(BigInteger firstCoeff, int t) {
		BigInteger[] coeffs = new BigInteger[t];
		coeffs[0] = firstCoeff;
		for (int i = 1; i < t; ++i) {
			// selects arbitrarily bj <- Zq
			coeffs[i] = new BigInteger(KEY_SPACE, RANDOM).mod(Q);
		}
		return coeffs;
	}

	public static IntFunction<BigInteger> polinomial(BigInteger[] coefficients) {
		return x -> {
			BigInteger b = coefficients[0];
			BigInteger bigX = BigInteger.valueOf(x);
			for (int i = 1; i < coefficients.length; ++i) {
				// b = b + c[i] * x ^ i
				b = b.add(coefficients[i].multiply(bigX.pow(i)));
			}
			return b;
		};
	}

	public static BinaryOperator<BigInteger> modMul(BigInteger mod) {
		return (x, y) -> x.multiply(y).mod(mod);
	}

}
