package by.sign.proxy.actor;

import static by.sign.proxy.ParamsHolder.G;
import static by.sign.proxy.ParamsHolder.KEY_SPACE;
import static by.sign.proxy.ParamsHolder.P;
import static by.sign.proxy.ParamsHolder.P_MINUS_ONE;
import static by.sign.proxy.ParamsHolder.Q;
import static by.sign.proxy.key.DsaKeyManagement.RANDOM;

import java.math.BigInteger;
import java.util.List;
import java.util.function.IntFunction;

import by.sign.proxy.artifact.Group;
import by.sign.proxy.artifact.Proxy;
import by.sign.proxy.artifact.ProxyShare;
import by.sign.proxy.artifact.Warrant;
import by.sign.proxy.key.DsaKeyPairWrapper;

public class OriginalSigner {
	private DsaKeyPairWrapper keyPair;

	public OriginalSigner(DsaKeyPairWrapper keyPairWrapper) {
		this.keyPair = keyPairWrapper;
	}

	public Proxy generateProxy(Warrant warrant) {
		// k <- Zp-1/{0}
		BigInteger k = new BigInteger(KEY_SPACE, RANDOM);

		// K = g^k mod P
		BigInteger K = G.modPow(k, P);

		// j = h(mw, K)
		BigInteger j = ProxyHelper.hash(warrant, K);
		BigInteger sk = keyPair.getX();

		// s = j * sa + k mod p - 1
		BigInteger sigm = j.multiply(sk).mod(P_MINUS_ONE);
		sigm = sigm.add(k).mod(P_MINUS_ONE);

		Proxy proxy = new Proxy(warrant, sigm, K, keyPair.getY());
		return proxy;
	}

	/**
	 * Set public values for share in the Group (Bj) and generate private proxy
	 * share for each member (bi)
	 */
	public void setProxySharesToGroup(Group group, Warrant warrant) {
		Proxy proxy = generateProxyToShare(warrant);
		int t = group.getThreshold();

		// proxy secret key is zero element in polynomial
		BigInteger firstCoeff = proxy.getSigm();
		BigInteger[] bj = ProxyHelper.randomCoefficients(firstCoeff, t);

		/*
		 * Bj <- g ^ bj
		 * here is tricky:
		 * during verification: Bj ^ (i ^ j) mod p => can exponentiate modulo p
		*/
		BigInteger[] Bj = new BigInteger[t - 1];
		for (int i = 0; i < t - 1; ++i) {
			Bj[i] = G.modPow(bj[i + 1], P);
		}
		group.setProxySharePublic(Bj);

		int n = group.getSize();
		List<ProxyThresholdSigner> members = group.getMembers();
		IntFunction<BigInteger> polinom = ProxyHelper.polinomial(bj);
		for (int i = 0; i < n; ++i) {
			int proxyShareIndex = i + 1;
			BigInteger secretProxyShare = polinom.apply(proxyShareIndex);
			ProxyShare proxyShare = new ProxyShare(group, proxy, secretProxyShare, proxyShareIndex);
			members.get(i).setProxyShare(proxyShare);
		}
	}

	private Proxy generateProxyToShare(Warrant warrant) {
		// k <- Zq
		BigInteger k = new BigInteger(KEY_SPACE, RANDOM).mod(Q);

		// K = g^k mod P
		BigInteger K = G.modPow(k, P);

		// j = h(mw, K)
		BigInteger j = ProxyHelper.hash(warrant, K);
		BigInteger sk = keyPair.getX();

		// s = j * sa + k mod q
		BigInteger sigm = j.multiply(sk).add(k).mod(Q);

		return new Proxy(warrant, sigm, K, keyPair.getY());
	}

}
