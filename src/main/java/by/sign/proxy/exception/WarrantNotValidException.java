package by.sign.proxy.exception;

public class WarrantNotValidException extends Exception {
	private static final long serialVersionUID = -3544981625049978838L;

	public WarrantNotValidException() {
	}

	public WarrantNotValidException(String message) {
		super(message);
	}

}
