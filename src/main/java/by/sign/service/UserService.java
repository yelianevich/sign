package by.sign.service;

import java.math.BigInteger;
import java.security.KeyPair;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import by.sign.controller.form.UserForm;
import by.sign.dao.IUserDao;
import by.sign.enums.Role;
import by.sign.model.ServiceUser;
import by.sign.proxy.key.DsaKeyManagement;
import by.sign.proxy.key.DsaKeyPairWrapper;

/**
 * @author Raman Yelianevich
 * @version 1.0
 */
public class UserService {
	private IUserDao userDao;

	public UserService() {
	}

	public ServiceUser getUser(String username) {
		return userDao.getUser(username);
	}

	public ServiceUser getUser(BigInteger y) {
		return userDao.getUser(y);
	}

	public ServiceUser getUserByPk(String y) {
		return userDao.getUserByPk(y);
	}

	public List<ServiceUser> getUsers() {
		return userDao.getUsers(1, 100);
	}

	public boolean checkCredentials(String username, String password) {
		ServiceUser user = userDao.getUser(username);
		if (user != null) {
			if (StringUtils.equals(username, password)) {
				return true;
			}
		}
		return false;
	}

	public boolean saveUser(UserForm user) {
		if (user == null || StringUtils.isBlank(user.getUsername())
				|| StringUtils.isBlank(user.getPassword())) {
			throw new IllegalArgumentException();
		}
		ServiceUser serviceUser = null; 
		if (user.isEditMode()) {
			serviceUser = user.getServiceUser();
		} else {
			
			// insert mode
			serviceUser = new ServiceUser(user.getUsername(), user.getPassword(), Role.ROLE_USER.getRole());

			// generate key pair for new user
			KeyPair keyPair = DsaKeyManagement.generateKeyPair();
			DsaKeyPairWrapper keyPairWrapper = new DsaKeyPairWrapper(keyPair);
			serviceUser.setX(keyPairWrapper.getX());
			serviceUser.setY(keyPairWrapper.getY());
		}
		serviceUser.setEmail(user.getEmail());
		serviceUser.setFirstName(user.getFirstName());
		serviceUser.setLastName(user.getLastName());
		serviceUser.setPosition(user.getPosition());
		user.setPassword(null);
		if (user.isEditMode()) {
			return userDao.saveUser(serviceUser);
		} else {
			return userDao.insertUser(serviceUser);
		}
	}
	
	public boolean deleteUser(String username) {
		return userDao.deleteUser(username);
	}

	public List<ServiceUser> findUser(String nameQuery) {
		return userDao.findUser(nameQuery, 10);
	}

	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}
}