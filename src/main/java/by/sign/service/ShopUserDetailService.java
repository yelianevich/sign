package by.sign.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import by.sign.dao.IUserDao;

/**
 * Custom implementation of UserDetailsService to get user from database.
 * 
 * @author Raman Yelianevich Yelianevich
 * 
 */
public class ShopUserDetailService implements UserDetailsService {
	private IUserDao userDao;

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		UserDetails userDetails = userDao.getUser(username);
		if (userDetails == null) {
			throw new UsernameNotFoundException("User " + username
					+ " not found.");
		}
		return userDetails;
	}

	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}
}
