package by.sign.service;

import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.List;

import org.apache.log4j.Logger;
import org.bouncycastle.util.Arrays;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.springframework.security.core.context.SecurityContextHolder;

import by.sign.controller.form.ValidationResult;
import by.sign.dao.IProxyDao;
import by.sign.enums.ProxyType;
import by.sign.model.ServiceUser;
import by.sign.proxy.DsaSignatureProcessor;
import by.sign.proxy.ParamsHolder;
import by.sign.proxy.actor.OriginalSigner;
import by.sign.proxy.actor.ProxySigner;
import by.sign.proxy.actor.Verifier;
import by.sign.proxy.artifact.Proxy;
import by.sign.proxy.artifact.SubstituteProxy;
import by.sign.proxy.artifact.Warrant;
import by.sign.proxy.exception.WarrantNotValidException;
import by.sign.proxy.key.DsaKeyManagement;
import by.sign.proxy.key.DsaKeyPairWrapper;

public class ProxyService {

	private static final Logger LOG = Logger.getLogger(ProxyService.class);
	private IProxyDao<Proxy> proxyDao;
	private IProxyDao<SubstituteProxy> subProxyDao;
	private UserService userService;
	private static final String SIGN_DELIMITER = ";;";
	private static final String DSA = "dsa";

	public ProxyService() {

	}

	public Proxy getProxy(String proxyId, ProxyType type) {
		Proxy proxy = null;
		if (type == ProxyType.PROTECTED) {
			proxy = subProxyDao.getProxy(proxyId, SubstituteProxy.class);
		} else {
			proxy = proxyDao.getProxy(proxyId, Proxy.class);
		}
		return proxy;
	}

	public boolean sendProxy(ServiceUser targetUser, DateTime from, Period period, ProxyType type) {
		Warrant warrant = new Warrant(from, period);
		ServiceUser user = (ServiceUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		DsaKeyPairWrapper keyPairWrapper = DsaKeyManagement.getKeyPair(user);

		OriginalSigner originalSigner = new OriginalSigner(keyPairWrapper);
		Proxy proxy = originalSigner.generateProxy(warrant);
		proxy.setTargetUser(targetUser.getUsername());
		if (type == ProxyType.PROTECTED) {
			DsaKeyPairWrapper proxyKeyPair = DsaKeyManagement.getKeyPair(targetUser);
			ProxySigner proxySigner = new ProxySigner(proxyKeyPair);
			boolean isNotValidProxy = !proxySigner.verifyProxy(proxy);
			if (isNotValidProxy) {
				LOG.info("Generated proxy did not pass proxy signer validation");
				return false;
			}
			proxy = proxySigner.generateSubstituteProxy(proxy);
		}
		boolean isAdded = proxyDao.addProxy(proxy, type);
		return isAdded;
	}

	public List<Proxy> getUnprotectedProxies() {
		List<Proxy> proxyList = null;
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		proxyList = proxyDao.getProxyList(1, 100, Proxy.class, ProxyType.UNPROTECTED, username);
		return proxyList;
	}

	public List<SubstituteProxy> getProtectedProxies() {
		List<SubstituteProxy> subProxyList = null;
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		subProxyList = subProxyDao.getProxyList(1, 100, SubstituteProxy.class, ProxyType.PROTECTED, username);
		return subProxyList;
	}

	public byte[] sign(Proxy proxy, ProxyType proxyType, byte[] content) throws WarrantNotValidException {
		byte[] signature = null;
		PrivateKey privateKey = null;
		String typeString = null;

		if (proxyType == null) {
			// DSA signature
			ServiceUser user = (ServiceUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			privateKey = DsaKeyManagement.getPrivateKey(user);
			typeString = SIGN_DELIMITER + DSA + SIGN_DELIMITER + user.getY().toString(ParamsHolder.KEY_RADIX);
		} else {
			Warrant warrant = proxy.getWarrant();
			if (!warrant.isValid()) {
				throw new WarrantNotValidException("Warrant is not valid: " + warrant.getLabel());
			}
			if (proxyType == ProxyType.UNPROTECTED) {
				privateKey = DsaKeyManagement.getUnprotectedPrivateKey(proxy);
				typeString = SIGN_DELIMITER + proxyType.getCode() + SIGN_DELIMITER + proxy.getProxyId();
			} else if (proxyType == ProxyType.PROTECTED) {
				if (!proxy.getClass().equals(SubstituteProxy.class)) {
					throw new IllegalArgumentException("proxyType " + proxyType + " do not correspond to proxy "
							+ proxy.getClass());
				}
				privateKey = DsaKeyManagement.getProtectedPrivateKey((SubstituteProxy) proxy);
				typeString = SIGN_DELIMITER + proxyType.getCode() + SIGN_DELIMITER + proxy.getProxyId();
			}
		}

		DsaSignatureProcessor signer = new DsaSignatureProcessor();
		signature = signer.sign(privateKey, content);
		byte[] signType = typeString.getBytes(ParamsHolder.SIGN_TYPE_CHARSET);
		signature = Arrays.concatenate(signature, signType);

		return signature;
	}

	public ValidationResult verify(byte[] content, byte[] signature) {
		ValidationResult result = new ValidationResult();

		if (content == null || signature == null) {
			throw new IllegalArgumentException("Params should be not null");
		}

		// preparation to parse signature
		String signStr = new String(signature, ParamsHolder.SIGN_TYPE_CHARSET);
		String[] signPart = signStr.split(SIGN_DELIMITER);

		// signature consist from 3 parts: signature, signature type, proxy ID or user public key
		if (signPart.length != 3) {
			new IllegalArgumentException("Signature has not valid format");
		}

		String typeCode = signPart[1];
		ProxyType signType = ProxyType.parseCode(typeCode);
		DsaSignatureProcessor processor = new DsaSignatureProcessor();
		if (signType == null) { // not proxy - DSA case
			String strY = signPart[2];
			ServiceUser signer = userService.getUserByPk(strY);

			BigInteger y = signer.getY();
			PublicKey pk = DsaKeyManagement.getPublicKey(y);

			result.setProxyType(null);
			result.setOriginalSigner(signer);
			result.setValid(processor.verify(pk, content, signature));
		} else if (signType == ProxyType.PROTECTED) {
			String proxyId = signPart[2];
			SubstituteProxy proxy = subProxyDao.getProxy(proxyId, SubstituteProxy.class);

			ServiceUser originalSigner = userService.getUser(proxy.getOriginalY());
			ServiceUser proxySigner = userService.getUser(proxy.getProxyY());

			BigInteger computedPk = Verifier.computeProtectedPublicKey(proxy);
			PublicKey pkToVerify = DsaKeyManagement.getPublicKey(computedPk);

			boolean isValidSignature = processor.verify(pkToVerify, content, signature);

			result.setWarrant(proxy.getWarrant());
			result.setOriginalSigner(originalSigner);
			result.setProxySigner(proxySigner);
			result.setProxyType(signType);
			result.setValid(isValidSignature);
		} else if (signType == ProxyType.UNPROTECTED) {
			String proxyId = signPart[2];
			Proxy proxy = proxyDao.getProxy(proxyId, Proxy.class);

			ServiceUser originalSigner = userService.getUser(proxy.getOriginalY());

			BigInteger computedPk = Verifier.computeUnprotectedPublicKey(proxy);
			PublicKey pkToVerify = DsaKeyManagement.getPublicKey(computedPk);

			boolean isValidSignature = processor.verify(pkToVerify, content, signature);

			result.setWarrant(proxy.getWarrant());
			result.setOriginalSigner(originalSigner);
			result.setProxyType(signType);
			result.setValid(isValidSignature);
		}
		return result;
	}

	public void setProxyDao(IProxyDao<Proxy> proxyDao) {
		this.proxyDao = proxyDao;
	}

	public void setSubProxyDao(IProxyDao<SubstituteProxy> subProxyDao) {
		this.subProxyDao = subProxyDao;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}
