package by.sign.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import by.sign.dao.ICompanyDao;
import by.sign.model.Company;

public class CompanyService {

	private ICompanyDao companyDao;

	public CompanyService() {
	}

	public boolean addCompany(Company company) {
		if (StringUtils.isEmpty(company.getCompanyId())) {
			return companyDao.addCompany(company);
		} else {
			return companyDao.updateCompany(company);
		}
			
	}

	public Company getCompany(String id) {
		return companyDao.getCompany(id);
	}

	public boolean updateCompany(Company company) {
		return companyDao.updateCompany(company);
	}

	public boolean deleteCompany(String id) {
		return companyDao.deleteCompany(id);
	}

	public void setCompanyDao(ICompanyDao companyDao) {
		this.companyDao = companyDao;
	}

	public List<Company> getCompaniesList() {
		return companyDao.getCompaniesList(1, 100);
	}
}
