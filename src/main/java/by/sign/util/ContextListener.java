package by.sign.util;

import java.security.Security;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.web.context.ContextLoaderListener;

/**
 * Initialize web application context path and perform bouncy castle initialization
 * 
 * @author Raman_Yelianevich
 */
@WebListener
public final class ContextListener extends ContextLoaderListener implements ServletContextListener {
	private static final Logger LOG = Logger.getLogger(ContextListener.class);

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ServletContext servletContext = sce.getServletContext();
		String contextPath = servletContext.getRealPath("/");
		String separator = System.getProperty("file.separator");
		String logsPath = contextPath + "WEB-INF" + separator;
		System.setProperty("logPath", logsPath);
		WebData.rootPath = contextPath;
		WebData.separator = separator;
		LOG.info("Context path: " + contextPath);

		// Add BouncyCastle to JCE as crypto provider
		Security.addProvider(new BouncyCastleProvider());
	}
}