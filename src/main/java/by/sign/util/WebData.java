package by.sign.util;

public final class WebData {
	private WebData() {
	}

	/** Path to project root (Came from context listener) */
	public static String rootPath;

	/** System separator (Came from context listener) */
	public static String separator;
}
