package by.sign.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.security.core.GrantedAuthority;

import by.sign.enums.Role;

public final class Utils {

	private Utils() { }
	
	public static Collection<String> getRoles(Collection<GrantedAuthority> auths) {
		List<String> roles = new ArrayList<>(auths.size());
		for (GrantedAuthority auth : auths) {
			roles.add(auth.getAuthority());
		}
		return roles;
	}
	
	public static boolean hasRole(Collection<? extends GrantedAuthority> auths, String role) {
		for (GrantedAuthority auth : auths) {
			if (StringUtils.equals(auth.getAuthority(), role)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean hasRole(Collection<? extends GrantedAuthority> auths, Role role) {
		return hasRole(auths, role.toString());
	}
	
	public static List<ObjectId> toObjectIds(List<String> strIds) {
		List<ObjectId> ids = new ArrayList<>(strIds.size());
		
		for (String strId : strIds) {
			ids.add(new ObjectId(strId));
		}
		return ids;
	}

}
