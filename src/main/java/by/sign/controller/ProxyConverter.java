package by.sign.controller;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import by.sign.enums.ProxyType;
import by.sign.proxy.artifact.Proxy;
import by.sign.proxy.artifact.SubstituteProxy;
import by.sign.service.ProxyService;

public class ProxyConverter implements Converter {

	private ProxyService proxyService;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		String[] proxyPart = value.split(";");
		String proxyId = proxyPart[0];
		ProxyType type = ProxyType.parseCode(proxyPart[1]);
		Proxy proxy = proxyService.getProxy(proxyId, type);
		return proxy;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		ProxyType type = null;

		if (value.getClass() == SubstituteProxy.class) {
			type = ProxyType.PROTECTED;
		} else {
			type = ProxyType.UNPROTECTED;
		}

		Proxy proxy = (Proxy) value;
		String proxyId = proxy.getProxyId() + ";" + type.getCode();
		return proxyId;
	}

	public void setProxyService(ProxyService proxyService) {
		this.proxyService = proxyService;
	}

}
