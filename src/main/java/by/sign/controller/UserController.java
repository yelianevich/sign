package by.sign.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import by.sign.controller.form.UserForm;
import by.sign.enums.Role;
import by.sign.model.ServiceUser;
import by.sign.service.UserService;
import by.sign.util.Utils;

/**
 * @author Raman Yelianevich
 * @version 1.0
 */
public class UserController implements Serializable {
	private static final long serialVersionUID = -8118462113323863022L;
	private static final Logger LOG = Logger.getLogger(UserController.class);
	private UserForm userForm;
	private String password;
	private String username;

	@ManagedProperty(value = "#{userService}")
	private UserService userService;

	@ManagedProperty(value = "#{authenticationManager}")
	private AuthenticationManager authenticationManager;

	public UserController() {
	}

	public String login() {
		try {
			Authentication request = new UsernamePasswordAuthenticationToken(this.getUsername(), this.getPassword());
			Authentication result = authenticationManager.authenticate(request);
			SecurityContextHolder.getContext().setAuthentication(result);
			this.setUsername(null);
			if (result.isAuthenticated() && Utils.hasRole(result.getAuthorities(), Role.ROLE_ADMIN)) {
				return "/pages/admin/admin?faces-redirect=true";
			}
		} catch (AuthenticationException e) {
			LOG.info("Authentivation failed. " + e.getMessage());
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Wrong credentials",
							"Username or password is incorrect"));
			return "/pages/pub/login";
		}
		return "/pages/user/home?faces-redirect=true";
	}

	public boolean checkRole(String role) {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal.getClass().equals(String.class)) {
			// if "anonimousUser" logged in
			String anonimousUser = (String) principal;
			return StringUtils.equals(anonimousUser, role);
		}
		UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		for (GrantedAuthority userRole : user.getAuthorities()) {
			if (userRole.getAuthority().equals(role)) {
				return true;
			}
		}
		return false;
	}
	
	public String toEditUser(String username) {
		ServiceUser su = userService.getUser(username);
		userForm = new UserForm(true);
		userForm.setServiceUser(su);
		userForm.setEmail(su.getEmail());
		userForm.setFirstName(su.getFirstName());
		userForm.setLastName(su.getLastName());
		userForm.setPassword(su.getPassword());
		userForm.setPosition(su.getPosition());
		userForm.setUsername(username);
		return "/pages/admin/editUser";
	}

	public String toAddUser() {
		userForm = new UserForm(false);
		return "/pages/admin/editUser";
	}

	public void saveUser() {
		boolean isSaved = userService.saveUser(userForm);
		FacesContext f = FacesContext.getCurrentInstance();
		if (isSaved) {
			String msg = "Successfully Saved";
			f.addMessage("messages", new FacesMessage(msg));
			userForm = new UserForm();
		} else {
			String msg = "Cannot Save. Username is not unique";
			f.addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", msg));
		}
	}
	
	public void deleteUser(String username) {
		boolean isDeleted = userService.deleteUser(username);
		FacesContext f = FacesContext.getCurrentInstance();
		if (isDeleted) {
			String msg = "Successfully Deleted";
			f.addMessage("messages", new FacesMessage(msg));
		} else {
			String msg = "Cannot Delete";
			f.addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", msg));
		}
	}

	public List<ServiceUser> getUsers() {
		return userService.getUsers();
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public UserForm getUserForm() {
		return userForm;
	}

	public void setUserForm(UserForm userForm) {
		this.userForm = userForm;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public void setAuthenticationManager(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}
}