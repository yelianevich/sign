package by.sign.controller;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import by.sign.controller.form.ValidationResult;
import by.sign.enums.ProxyType;
import by.sign.model.ServiceUser;
import by.sign.proxy.artifact.Proxy;
import by.sign.proxy.exception.WarrantNotValidException;
import by.sign.service.ProxyService;
import by.sign.service.UserService;

public class SignController implements Serializable {
	private static final long serialVersionUID = 9101093130933367019L;

	private ProxyService proxyService;
	private UserService userService;
	private FileUploadHandler fileUpload;
	private ProxyConverter proxyConverter;
	private Proxy proxy;
	private ProxyType proxyType;
	private StreamedContent file;
	private ValidationResult result;

	public SignController() {
	}

	public void signDocUsingProxy() {
		byte[] signature = null;
		try {
			signature = proxyService.sign(proxy, proxyType, fileUpload.getFile());
			InputStream stream = new ByteArrayInputStream(signature);
			file = new DefaultStreamedContent(stream, "application/octet-stream", fileUpload.getFileName() + ".sign");
			FacesMessage msg = new FacesMessage(fileUpload.getFileName() + " signed");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (WarrantNotValidException e) {
			FacesMessage msg = new FacesMessage("Warrant is not valid for specified proxy");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
	}

	public void signDsa() {
		FacesMessage msg = new FacesMessage(proxyType + " " + proxy.getTargetUser());
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void verifySignature() {
		result = proxyService.verify(fileUpload.getFile(), fileUpload.getSignature());

		FacesMessage msg = new FacesMessage(result.isValid() ? "Valid" : "Not Valid");
		FacesContext.getCurrentInstance().addMessage("introPanel:growl", msg);
	}

	public List<? extends Proxy> getProxyList() {
		List<? extends Proxy> proxyList = null;
		if (proxyType == ProxyType.PROTECTED) {
			proxyList = proxyService.getProtectedProxies();
		} else if (proxyType == ProxyType.UNPROTECTED) {
			proxyList = proxyService.getUnprotectedProxies();
		}
		return proxyList;
	}

	public String getOriginalUserLabel(Proxy proxy) {
		ServiceUser user = userService.getUser(proxy.getOriginalY());
		String label = user.getFullName() + " (" + user.getUsername() + ") - " + proxy.getWarrant().getLabel();
		return label;
	}

	public String toProtectedProxy() {
		proxyType = ProxyType.PROTECTED;
		return "/pages/user/sign?faces-redirect=true";
	}

	public String toUnprotectedProxy() {
		proxyType = ProxyType.UNPROTECTED;
		return "/pages/user/sign?faces-redirect=true";
	}

	public String toDsa() {
		proxyType = null;
		return "/pages/user/sign?faces-redirect=true";
	}

	public void setFileUpload(FileUploadHandler fileUpload) {
		this.fileUpload = fileUpload;
	}

	public FileUploadHandler getFileUpload() {
		return fileUpload;
	}

	public StreamedContent getFile() {
		return file;
	}

	public ProxyType getProxyType() {
		return proxyType;
	}

	public void setProxyType(ProxyType proxyType) {
		this.proxyType = proxyType;
	}

	public ProxyService getProxyService() {
		return proxyService;
	}

	public void setProxyService(ProxyService proxyService) {
		this.proxyService = proxyService;
	}

	public Proxy getProxy() {
		return proxy;
	}

	public void setProxy(Proxy proxy) {
		this.proxy = proxy;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public ProxyConverter getProxyConverter() {
		return proxyConverter;
	}

	public void setProxyConverter(ProxyConverter proxyConverter) {
		this.proxyConverter = proxyConverter;
	}

	public ValidationResult getResult() {
		return result;
	}
}
