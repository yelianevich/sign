package by.sign.controller;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

public class FileUploadHandler implements Serializable {
	private static final long serialVersionUID = -7701827152008208292L;
	private static final Logger LOG = Logger.getLogger(FileUploadHandler.class);

	private String fileName;
	private byte[] file;
	private boolean fileUploaded;

	private String signatureName;
	private byte[] signature;
	private boolean signatureUploaded;

	public void handleFileUpload(FileUploadEvent event) {
		FacesMessage fileMsg = null;
		UploadedFile f = event.getFile();
		file = new byte[(int) f.getSize()];
		fileName = f.getFileName();

		this.fileUploaded = handleFileEvent(event, file);
		if (fileUploaded) {
			String strFileMsg = fileName + " is ready for processing.";
			fileMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "File uploaded", strFileMsg);
		} else {
			String strFileMsg = "Error occured during uploading." + fileName;
			fileMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", strFileMsg);
		}
		FacesContext.getCurrentInstance().addMessage("fileForm:fileMsg", fileMsg);
	}

	public void handleSignatureUpload(FileUploadEvent event) {
		FacesMessage fileMsg = null;
		UploadedFile f = event.getFile();
		signature = new byte[(int) f.getSize()];
		signatureName = f.getFileName();

		signatureUploaded = handleFileEvent(event, signature);
		if (signatureUploaded) {
			fileMsg = new FacesMessage("Signature " + signatureName + " is uploaded");
		} else {
			String strFileMsg = "Error occured during uploading." + signatureName;
			fileMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", strFileMsg);
		}
		FacesContext.getCurrentInstance().addMessage("signForm:signMsg", fileMsg);
	}

	private boolean handleFileEvent(FileUploadEvent event, byte[] content) {
		UploadedFile f = event.getFile();
		boolean contentUploaded = false;

		try (InputStream stream = f.getInputstream(); BufferedInputStream inputStream = new BufferedInputStream(stream);) {
			inputStream.read(content);
			contentUploaded = true;
		} catch (IOException e) {
			LOG.error("Error occured during file uploading", e);
		}
		return contentUploaded;
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

	public boolean isFileUploaded() {
		return fileUploaded;
	}

	public void setFileUploaded(boolean fileUploaded) {
		this.fileUploaded = fileUploaded;
	}

	public byte[] getSignature() {
		return signature;
	}

	public void setSignature(byte[] signature) {
		this.signature = signature;
	}

	public boolean isSignatureUploaded() {
		return signatureUploaded;
	}

	public void setSignatureUploaded(boolean signatureUploaded) {
		this.signatureUploaded = signatureUploaded;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getSignatureName() {
		return signatureName;
	}

	public void setSignatureName(String signatureName) {
		this.signatureName = signatureName;
	}

	@Override
	public String toString() {
		return "FileUploadHandler [fileUploaded=" + fileUploaded + ", signatureUploaded=" + signatureUploaded + "]";
	}

}
