package by.sign.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.joda.time.DateTime;
import org.joda.time.Period;

import by.sign.controller.form.ProxyForm;
import by.sign.model.ServiceUser;
import by.sign.service.ProxyService;
import by.sign.service.UserService;

public class ProxyController implements Serializable {
	private static final long serialVersionUID = -1066354690205221298L;

	private ProxyService proxyService;
	private UserService userService;
	private ProxyForm proxyForm;

	public ProxyController() {
	}

	public List<ServiceUser> completeUser(String nameQuery) {
		List<ServiceUser> suggestions = userService.findUser(nameQuery);
		return suggestions;
	}

	public String toGenerateProxy() {
		proxyForm = new ProxyForm();
		return "/pages/user/editProxy?faces-redirect=true";
	}

	public void sendProxy() {
		DateTime from = new DateTime(proxyForm.getValidFrom());
		Period period = new Period().withDays(proxyForm.getValidDays());
		boolean proxySent = proxyService.sendProxy(proxyForm.getTargetUser(), from, period, proxyForm.getProxyType());
		String msg = proxySent ? "Sent!" : "Was not able to send proxy.";
		FacesContext f = FacesContext.getCurrentInstance();
		f.addMessage("messages", new FacesMessage(msg));
		if (proxySent) {
			proxyForm = new ProxyForm();
		}
	}

	public void setProxyService(ProxyService proxyService) {
		this.proxyService = proxyService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public ProxyForm getProxyForm() {
		return proxyForm;
	}

	public void setProxyForm(ProxyForm proxyForm) {
		this.proxyForm = proxyForm;
	}

}
