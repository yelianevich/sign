package by.sign.controller;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;

import by.sign.model.Company;
import by.sign.model.ServiceUser;
import by.sign.service.CompanyService;
import by.sign.service.UserService;

public class CompanyController {

	private Company company;
	private UserService userService;
	private CompanyService companyService;

	public CompanyController() {
	}

	public void addCompany() {
		boolean addMode = StringUtils.isBlank(company.getCompanyId());
		boolean isAdded = companyService.addCompany(company);
		String msg = isAdded ? "Saved!" : "Not able to add company.";
		FacesContext f = FacesContext.getCurrentInstance();
		f.addMessage("messages", new FacesMessage(msg));
		if (isAdded && addMode) {
			company = new Company();
		}
	}

	public String toAddCompany() {
		company = new Company();
		return "/pages/admin/editCompany";
	}

	public String toEditCompany(String id) {
		company = companyService.getCompany(id);
		return "/pages/admin/editCompany";
	}

	public void deleteCompany(String id) {
		boolean isDeleted = companyService.deleteCompany(id);

		FacesContext f = FacesContext.getCurrentInstance();
		if (isDeleted) {
			String msg = "Successfully Deleted";
			f.addMessage("messages", new FacesMessage(msg));
		} else {
			String msg = "Cannot Delete";
			f.addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Internal Error", msg));
		}
	}

	public List<Company> getCompanies() {
		return companyService.getCompaniesList();
	}

	public List<ServiceUser> completeUser(String nameQuery) {
		List<ServiceUser> suggestions = userService.findUser(nameQuery);
		return suggestions;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
