package by.sign.controller;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.apache.commons.lang3.StringUtils;

import by.sign.model.ServiceUser;
import by.sign.service.UserService;

public class UserConverter implements Converter {

	private UserService userService;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String username) {
		if (StringUtils.isNotBlank(username)) {
			return userService.getUser(username);
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null || value.equals(StringUtils.EMPTY)) {
			return StringUtils.EMPTY;
		} else {
			return ((ServiceUser) value).getUsername();
		}
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
