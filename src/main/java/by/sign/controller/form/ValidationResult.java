package by.sign.controller.form;

import by.sign.enums.ProxyType;
import by.sign.model.ServiceUser;
import by.sign.proxy.artifact.Warrant;

public class ValidationResult {

	private ProxyType proxyType;
	private ServiceUser originalSigner;
	private ServiceUser proxySigner;
	private Warrant warrant;
	private boolean valid;

	public ValidationResult() {
	}

	public ProxyType getProxyType() {
		return proxyType;
	}

	public void setProxyType(ProxyType proxyType) {
		this.proxyType = proxyType;
	}

	public ServiceUser getOriginalSigner() {
		return originalSigner;
	}

	public void setOriginalSigner(ServiceUser originalSigner) {
		this.originalSigner = originalSigner;
	}

	public ServiceUser getProxySigner() {
		return proxySigner;
	}

	public void setProxySigner(ServiceUser proxySigner) {
		this.proxySigner = proxySigner;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	@Override
	public String toString() {
		return "ValidationResult [proxyType=" + proxyType + ", originalSigner=" + originalSigner + ", proxySigner="
				+ proxySigner + ", valid=" + valid + "]";
	}

	public Warrant getWarrant() {
		return warrant;
	}

	public void setWarrant(Warrant warrant) {
		this.warrant = warrant;
	}

}
