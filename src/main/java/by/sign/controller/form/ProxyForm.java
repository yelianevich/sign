package by.sign.controller.form;

import java.io.Serializable;
import java.util.Date;

import by.sign.enums.ProxyType;
import by.sign.model.ServiceUser;

public class ProxyForm implements Serializable {
	private static final long serialVersionUID = -4201553523388131159L;

	private ProxyType proxyType = ProxyType.UNPROTECTED;
	private Date validFrom;
	private int validDays = 100;
	private ServiceUser targetUser;

	public ProxyType[] getProxyTypes() {
		return ProxyType.values();
	}

	public ProxyType getProxyType() {
		return proxyType;
	}

	public void setProxyType(ProxyType proxyType) {
		this.proxyType = proxyType;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public int getValidDays() {
		return validDays;
	}

	public void setValidDays(int validDays) {
		this.validDays = validDays;
	}

	public ServiceUser getTargetUser() {
		return targetUser;
	}

	public void setTargetUser(ServiceUser targetUser) {
		this.targetUser = targetUser;
	}

	@Override
	public String toString() {
		return "ProxyForm [proxyType=" + proxyType + ", validFrom=" + validFrom + ", validDays=" + validDays
				+ ", targetUser=" + targetUser + "]";
	}

}
