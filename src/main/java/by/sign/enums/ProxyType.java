package by.sign.enums;

import java.util.HashMap;
import java.util.Map;

public enum ProxyType {
	PROTECTED("protected_proxy", "Protected"), UNPROTECTED("unprotected_proxy", "Unprotected");

	private String code;
	private String label;
	private static final Map<String, ProxyType> reverseMap = new HashMap<>();

	static {
		for (ProxyType type : ProxyType.values()) {
			reverseMap.put(type.getCode(), type);
		}
	}

	private ProxyType(String code, String label) {
		this.code = code;
		this.label = label;
	}

	public static ProxyType parseCode(String code) {
		return reverseMap.get(code);
	}

	public String getLabel() {
		return label;
	}

	public String getCode() {
		return code;
	}
}
