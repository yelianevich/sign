package by.sign.enums;

import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public enum Role {
	ROLE_USER("ROLE_USER"), 
	ROLE_ADMIN("ROLE_ADMIN");
	
	private Set<GrantedAuthority> roles;
	private String roleName;
	
	private Role(String roleName) {
		this.roles = new HashSet<>();
		this.roles.add(new SimpleGrantedAuthority(roleName));
		this.roleName = roleName;
	}
	
	public String getName() {
		return roleName;
	}
	
	public Set<GrantedAuthority> getRole() {
		return roles;
	}
}
