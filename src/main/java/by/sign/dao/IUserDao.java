package by.sign.dao;

import java.math.BigInteger;
import java.util.List;

import by.sign.model.ServiceUser;
import by.sign.proxy.ParamsHolder;

/**
 * @author Raman Yelianevich
 * @version 1.0
 * 
 */
public interface IUserDao {

	/**
	 * Gets {@link ServiceUser} by his public key
	 * 
	 * @return user instance with populated fields or empty object if it is missing
	 */
	ServiceUser getUser(BigInteger y);

	/**
	 * Gets {@link ServiceUser} by his public key in format specified by {@link ParamsHolder#KEY_RADIX}
	 * 
	 * @return user instance with populated fields or empty object if it is missing
	 */
	ServiceUser getUserByPk(String y);

	/**
	 * Gets {@link ServiceUser} by username
	 * 
	 * @return user instance with populated fields or empty object if it was missing
	 */
	ServiceUser getUser(String username);
	
	/**
	 * Gets list of users for given list of usernames
	 */
	List<ServiceUser> getUsers(List<String> usernames);
	
	/**
	 * Gets range of users
	 * @param firstRecord - ordinal number of first record to return (starts from 1)
	 * @param lastRecord - ordinal number of last record to return (starts from 1)
	 */
	List<ServiceUser> getUsers(int firstRecord, int lastRecord);

	/**
	 * Saves or updates user in data storage
	 * @param user 
	 * @return true if user was saved, false otherwise
	 */
	boolean saveUser(ServiceUser user);

	boolean insertUser(ServiceUser user);
	
	/**
	 * Deletes user in data storage
	 * @param username
	 * @return 
	 */
	boolean deleteUser(String username);

	/**
	 * 
	 * @param nameQuery - user full name in the following format "FirstName LastName"
	 * @param limit - max number of records to return
	 */
	List<ServiceUser> findUser(String nameQuery, int limit);

}