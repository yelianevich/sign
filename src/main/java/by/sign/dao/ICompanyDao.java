package by.sign.dao;

import java.util.List;

import by.sign.model.Company;

/**
 * CRUD operations for {@link Company} model
 * @author Raman Yelianevich
 *
 */
public interface ICompanyDao {
	
	boolean addCompany(Company company);
	
	Company getCompany(String id);
	
	boolean updateCompany(Company company);
	
	boolean deleteCompany(String id);

	List<Company> getCompaniesList(int firstPos, int lastPos);

}
