package by.sign.dao;

import java.util.List;

import by.sign.enums.ProxyType;
import by.sign.proxy.artifact.Proxy;

public interface IProxyDao<T extends Proxy> {

	boolean addProxy(T proxy, ProxyType type);

	T getProxy(String id, Class<T> clazz);

	boolean updateProxy(T proxy);

	boolean deleteProxy(String id);

	List<T> getProxyList(int firstPos, int lastPos, Class<T> clazz, ProxyType type, String targetUser);

}
