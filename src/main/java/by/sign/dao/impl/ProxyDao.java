package by.sign.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;

import by.sign.dao.IProxyDao;
import by.sign.enums.ProxyType;
import by.sign.model.mapper.IModelMapper;
import by.sign.proxy.artifact.Proxy;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoException;

public class ProxyDao<T extends Proxy> implements IProxyDao<T> {
	private static final Logger LOG = Logger.getLogger(ProxyDao.class);
	private DBCollection proxies;
	private IModelMapper<T> mapper;

	@Override
	public boolean addProxy(T proxy, ProxyType type) {
		DBObject dbObj = mapper.toDbObject(proxy);
		ObjectId id = new ObjectId();
		dbObj.put("_id", id);
		dbObj.put("type", type.getCode());
		try {
			proxies.insert(dbObj);
			proxy.setProxyId(id.toString());
		} catch (MongoException e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
		return true;
	}

	@Override
	public T getProxy(String id, Class<T> clazz) {
		T proxy = null;
		DBObject dbObj = proxies.findOne(new BasicDBObject("_id", new ObjectId(id)));
		if (dbObj != null) {
			try {
				proxy = clazz.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				LOG.error(e.getMessage(), e);
			}
			mapper.restoreModel(dbObj, proxy);
		}
		return proxy;
	}

	@Override
	public boolean updateProxy(T proxy) {
		DBObject dbObj = mapper.toDbObject(proxy);
		DBObject query = new BasicDBObject("_id", dbObj.get("_id"));
		try {
			proxies.update(query, dbObj);
		} catch (MongoException e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
		return true;
	}

	@Override
	public boolean deleteProxy(String id) {
		boolean isDeleted = true;
		try {
			proxies.remove(new BasicDBObject("_id", new ObjectId(id)));
		} catch (MongoException e) {
			LOG.error(e.getMessage(), e);
			isDeleted = false;
		}
		return isDeleted;
	}

	@Override
	public List<T> getProxyList(int firstPos, int lastPos, Class<T> clazz, ProxyType type, String targerUser) {
		if (firstPos >= lastPos || lastPos < 1 || firstPos < 1) {
			throw new IllegalArgumentException("Rande [" + firstPos + "," + lastPos + "] is not correct");
		}

		List<T> proxyList = new ArrayList<>();
		DBObject query = new BasicDBObject("targetUser", targerUser).append("type", type.getCode());
		DBCursor dbProxies = proxies.find(query).limit(lastPos - firstPos + 1).skip(firstPos - 1);
		while (dbProxies.hasNext()) {
			T proxy = null;
			try {
				proxy = clazz.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				LOG.error(e.getMessage(), e);
			}
			mapper.restoreModel(dbProxies.next(), proxy);
			proxyList.add(proxy);
		}
		return proxyList;
	}

	public void setProxies(DBCollection proxies) {
		this.proxies = proxies;
	}

	public void setMapper(IModelMapper<T> mapper) {
		this.mapper = mapper;
	}

}
