package by.sign.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import by.sign.dao.IUserDao;
import by.sign.model.ServiceUser;
import by.sign.model.mapper.IModelMapper;
import by.sign.proxy.ParamsHolder;
import by.sign.util.Utils;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoException;
import com.mongodb.WriteResult;

/**
 * @author Raman Yelianevich
 * @version 1.0
 */
public class UserDao implements IUserDao {
	private static final Logger LOG = Logger.getLogger(UserDao.class);
	private DBCollection users;
	private IModelMapper<ServiceUser> mapper;

	public UserDao() {
	}

	@Override
	public ServiceUser getUser(String username) {
		ServiceUser serviceUser = null;
		DBObject user = users.findOne(new BasicDBObject("_id", username));
		if (user != null) {
			serviceUser = loadServiceUser(user);
		}
		return serviceUser;
	}
	
	@Override
	public ServiceUser getUser(BigInteger y) {
		return getUserByPk(y.toString(ParamsHolder.KEY_RADIX));
	}

	@Override
	public ServiceUser getUserByPk(String y) {
		ServiceUser serviceUser = null;
		DBObject user = users.findOne(new BasicDBObject("y", y));
		if (user != null) {
			serviceUser = loadServiceUser(user);
		}
		return serviceUser;
	}

	@Override
	public List<ServiceUser> findUser(String nameQuery, int limit) {
		List<ServiceUser> usersFound = new ArrayList<>(limit);
		String[] nameParts = nameQuery.split(" ");
		BasicDBList orList = new BasicDBList();
		if (nameParts.length > 0 && StringUtils.isNotBlank(nameParts[0])) {
			BasicDBObject fnQuery = new BasicDBObject("firstName", Pattern.compile(nameParts[0]));
			orList.add(fnQuery);
		}
		if (nameParts.length > 1 && StringUtils.isNotBlank(nameParts[1])) {
			BasicDBObject lnQuery = new BasicDBObject("lastName", Pattern.compile(nameParts[1]));
			orList.add(lnQuery);
		}
		if (!orList.isEmpty()) {
			BasicDBObject fnQuery = new BasicDBObject("$or", orList);
			DBCursor cursor = users.find(fnQuery).limit(limit);
			while (cursor.hasNext()) {
				usersFound.add(loadServiceUser(cursor.next()));
			}
		}
		return usersFound;
	}

	@Override
	public List<ServiceUser> getUsers(List<String> usernames) {
		List<ServiceUser> usersFound = new ArrayList<ServiceUser>();
		DBObject query = new BasicDBObject();
		
		if (usernames.size() == 1) {
			query.put("_id", usernames.get(0));
		} else {
			query.put("_id", new BasicDBObject("$in", usernames));
		}
		
		DBCursor cursor = users.find(query);
		while (cursor.hasNext()) {
			usersFound.add(loadServiceUser(cursor.next()));
		}
		return usersFound;
	}
	
	@Override
	public List<ServiceUser> getUsers(int firstPos, int lastPos) {
		if (firstPos >= lastPos || lastPos < 1 || firstPos < 1) {
			throw new IllegalArgumentException();
		}
		
		List<ServiceUser> usersFound = new ArrayList<ServiceUser>();
		
		// order by first name, than last name (ascending)
		DBObject orderBy = new BasicDBObject("firstName", 1).append("lastName", 1);
		DBCursor cursor = users.find().limit(lastPos - firstPos + 1).skip(firstPos - 1).sort(orderBy);
		while (cursor.hasNext()) {
			usersFound.add(loadServiceUser(cursor.next()));
		}
		return usersFound;
	}
	
	@Override
	public boolean insertUser(ServiceUser serviceUser) {
		DBObject user = toDbObject(serviceUser);
		try {
			users.insert(user);
			return true;
		} catch (MongoException e) {
			LOG.info(e.getMessage(), e);
			return false;
		}
	}

	@Override
	public boolean saveUser(ServiceUser serviceUser) {
		DBObject user = toDbObject(serviceUser);
		WriteResult writeResult = null;
		try {
			DBObject query = new BasicDBObject("_id", serviceUser.getUsername());
			
			// upsert user
			writeResult = users.update(query, user, true, false);
			return true;
		} catch (MongoException e) {
			LOG.info(writeResult.getError(), e);
			return false;
		}
	}

	@Override
	public boolean deleteUser(String username) {
		try {	
			users.remove(new BasicDBObject("_id", username));
		} catch (MongoException e) {
			LOG.info(e.getMessage(), e);
			return false;
		}
		return true;
	}

	public void setUsersCollection(DBCollection usersCollection) {
		this.users = usersCollection;
	}

	private DBObject toDbObject(ServiceUser su) {
		DBObject u = mapper.toDbObject(su);
		u.put("_id", su.getUsername());
		u.put("password", su.getPassword());
		u.put("roles", Utils.getRoles(su.getAuthorities()));
		return u;
	}

	private ServiceUser loadServiceUser(DBObject dbUser) {
		Set<GrantedAuthority> authorities = new HashSet<>();
		BasicDBList roles = (BasicDBList) dbUser.get("roles");
		for (Object role : roles) {
			authorities.add(new SimpleGrantedAuthority(role.toString()));
		}
		ServiceUser serviceUser = new ServiceUser(dbUser.get("_id").toString(), dbUser.get(
				"password").toString(), authorities);
		return mapper.restoreModel(dbUser, serviceUser);
	}

	public void setMapper(IModelMapper<ServiceUser> mapper) {
		this.mapper = mapper;
	}

}