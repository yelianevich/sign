package by.sign.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;

import by.sign.dao.ICompanyDao;
import by.sign.dao.IUserDao;
import by.sign.model.Company;
import by.sign.model.ServiceUser;
import by.sign.model.mapper.IModelMapper;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoException;

public class CompanyDao implements ICompanyDao {
	private static final Logger LOG = Logger.getLogger(CompanyDao.class);
	private DBCollection companies;
	private IModelMapper<Company> mapper;
	private IUserDao userDao;

	public CompanyDao() {
	}

	@Override
	public boolean addCompany(Company company) {
		DBObject dbObj = toDbObject(company);
		ObjectId id = new ObjectId();
		dbObj.put("_id", id);
		try {
			companies.insert(dbObj);
			company.setCompanyId(id.toString());
		} catch (MongoException e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
		return true;
	}

	@Override
	public Company getCompany(String id) {
		DBObject dbObj = companies.findOne(new BasicDBObject("_id", new ObjectId(id)));
		Company company = new Company();
		if (dbObj != null) {
			loadCompany(dbObj, company);
		}
		return company;
	}

	private void loadCompany(DBObject dbObj, Company company) {
		mapper.restoreModel(dbObj, company);

		@SuppressWarnings("unchecked")
		List<String> userIds = (List<String>) dbObj.get("users");
		List<ServiceUser> users;
		if (userIds != null) {
			users = userDao.getUsers(userIds);
		} else {
			users = new ArrayList<>();
		}
			
		company.setUsers(users);
	}

	@Override
	public List<Company> getCompaniesList(int firstPos, int lastPos) {
		if (firstPos >= lastPos || lastPos < 1 || firstPos < 1) {
			throw new IllegalArgumentException();
		}
		
		List<Company> companyList = new ArrayList<>();
		DBObject orderBy = new BasicDBObject("name", 1).append("maxUsers", 1);
		DBCursor dbCompanies = companies.find().limit(lastPos - firstPos + 1).skip(firstPos - 1).sort(orderBy);
		while (dbCompanies.hasNext()) {
			Company company = new Company();
			loadCompany(dbCompanies.next(), company);
			companyList.add(company);
		}
		return companyList;
	}

	@Override
	public boolean updateCompany(Company company) {
		DBObject dbObj = toDbObject(company);
		DBObject query = new BasicDBObject("_id", dbObj.get("_id"));
		try {
			companies.update(query, dbObj);
		} catch (MongoException e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
		return true;
	}

	@Override
	public boolean deleteCompany(String id) {
		boolean isDeleted = true;
		try {	
			companies.remove(new BasicDBObject("_id", new ObjectId(id)));
		} catch (MongoException e) {
			LOG.error(e.getMessage(), e);
			isDeleted = false;
		}
		return isDeleted;
	}
	
	private DBObject toDbObject(Company company) {
		DBObject dbObj = mapper.toDbObject(company);
		dbObj.put("users", toIdsList(company.getUsers()));
		return dbObj;
	}

	private List<String> toIdsList(List<ServiceUser> users) {
		List<String> userIds = new ArrayList<String>();
		
		if (users != null) {
			for (ServiceUser u : users) {
				userIds.add(u.getUsername());
			}
		}
		return userIds;
	}

	public void setCompanies(DBCollection companies) {
		this.companies = companies;
	}

	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}

	public void setMapper(IModelMapper<Company> mapper) {
		this.mapper = mapper;
	}
}
