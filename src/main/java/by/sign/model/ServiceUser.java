package by.sign.model;

import java.math.BigInteger;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import by.sign.model.mapper.MapperModel;
import by.sign.model.mapper.Prop;

/**
 * User of sign service. Belongs to specific company, except service administrator.
 * Model contains DSA public and private keys
 * 
 * @author Raman Yelianevich
 * @version 1.0
 */
@MapperModel
public class ServiceUser extends User {
	private static final long serialVersionUID = 809204807080927669L;

	@Prop
	private String email;

	@Prop
	private String firstName;

	@Prop
	private String lastName;

	@Prop
	private String position;

	@Prop
	private BigInteger x;

	@Prop
	private BigInteger y;

	public ServiceUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public BigInteger getY() {
		return y;
	}

	public void setY(BigInteger y) {
		this.y = y;
	}

	public BigInteger getX() {
		return x;
	}

	public void setX(BigInteger x) {
		this.x = x;
	}

	public String getFullName() {
		return firstName + " " + lastName;
	}

	@Override
	public String toString() {
		return "ServiceUser [firstName=" + firstName + ", lastName=" + lastName + ", getUsername()=" + getUsername()
				+ "]";
	}

}