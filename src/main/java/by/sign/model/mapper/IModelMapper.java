package by.sign.model.mapper;

import com.mongodb.DBObject;

/**
 * Can map {@link DBObject} to {@code T} model type and vice versa
 * @author Raman Yelianevich
 * @param <T> - model type
 */
public interface IModelMapper <T> {
	
	/**
	 * Map model to {@link DBObject}. Each field should be mapped with
	 * {@link prop} annotation.
	 */
	DBObject toDbObject(T model);
	
	/**
	 * Restore model annotated with {@link MapperModel}. Each field that should
	 * be populated marked with {@link Prop} annotation.
	 * 
	 * @param model
	 *            - empty model object
	 * @param dbObj
	 *            - instance with properties to be resored
	 * @return populated {@code model}
	 */
	T restoreModel(DBObject dbObj, T model);
}
