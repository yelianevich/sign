package by.sign.model.mapper;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * States that field should be serialized to DbObject.
 * If value not specified, that field name should be used as key.
 * 
 * @author Raman Yelianevich
 *
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Prop {
	String value() default "";
}
