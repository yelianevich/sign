package by.sign.model.mapper;

import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;

import by.sign.proxy.artifact.Warrant;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * Generic approach to map model to MongoDB {@link DBObject}
 * 
 * @author Raman Yelianevich
 * @param <T>
 *            model type
 * 
 */
public class ModelMapper<T> implements IModelMapper<T> {
	private static final int RADIX = 16;
	private static final Logger LOG = Logger.getLogger(MapperModel.class);

	@Override
	public DBObject toDbObject(T model) {
		if (model == null) {
			throw new IllegalArgumentException("Model could not be null");
		}

		DBObject dbObj = new BasicDBObject();
		List<Class<?>> hierarchy = getModelHierarchy(model);
		for (Class<?> clazz : hierarchy) {
			for (Field field : clazz.getDeclaredFields()) {
				Prop prop = field.getAnnotation(Prop.class);
				if (prop != null) {
					// field processing
					String key = prop.value();
					if (StringUtils.isBlank(key)) {
						key = field.getName();
					}
					Object value = null;
					try {
						field.setAccessible(true);
						value = field.get(model);
						value = serializeFieldValue(key, value);
					} catch (Exception e) {
						LOG.error("ModelMapper was not able to get value from the field", e);
					}
					if (value != null) {
						dbObj.put(key, value);
					}
				}
			}
		}
		return dbObj;
	}

	private Object serializeFieldValue(String key, Object value) {
		if (StringUtils.equals(key, "_id") && value != null) {
			value = new ObjectId(value.toString());
		} else if (value instanceof BigInteger) {
			// save BigInteger fields as string
			value = ((BigInteger) value).toString(RADIX);
		} else if (value instanceof Warrant) {
			value = ((Warrant) value).toString();
		}
		return value;
	}

	private List<Class<?>> getModelHierarchy(T model) {
		List<Class<?>> classes = new ArrayList<>();
		for (Class<?> clazz = model.getClass(); (clazz != null) && (clazz != Object.class); clazz = clazz
				.getSuperclass()) {
			if (clazz.isAnnotationPresent(MapperModel.class)) {
				classes.add(clazz);
			}
		}
		return classes;
	}

	@Override
	public T restoreModel(DBObject dbObj, T model) {
		if (model == null) {
			throw new IllegalArgumentException("model could not be null");
		} else if (dbObj == null) {
			throw new IllegalArgumentException("dbObj could not be null");
		}

		List<Class<?>> hierarchy = getModelHierarchy(model);
		for (Class<?> clazz : hierarchy) {
			for (Field field : clazz.getDeclaredFields()) {
				Prop prop = field.getAnnotation(Prop.class);
				if (prop != null) {
					String key = prop.value();
					if (StringUtils.isBlank(key)) {
						key = field.getName();
					}
					Object value = dbObj.get(key);
					if (value != null) {
						field.setAccessible(true);
						deserializeValue(field, model, key, value);
					}
				}
			}
		}
		return model;
	}

	private void deserializeValue(Field field, T model, String key, Object value) {
		Class<?> fieldType = field.getType();
		try {
			if (StringUtils.equals(key, "_id")) {
				field.set(model, value.toString());
			} else if (fieldType.equals(BigInteger.class)) {
				field.set(model, new BigInteger((String) value, RADIX));
			} else if (value instanceof Number) {
				// MongoDB stores all numbers in double format, therefore cast is required
				field.set(model, toFieldType(field, value));
			} else if (fieldType.equals(Warrant.class)) {
				Warrant warrant = Warrant.valueOf((String) value);
				field.set(model, warrant);
			} else {
				// field deserialized by Mongo to appropriate type
				field.set(model, value);
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			LOG.error(e.getMessage(), e);
		}
	}

	private Number toFieldType(Field field, Object value) {
		Class<?> fieldClass = field.getType();
		Number val = (Number) value;
		if (fieldClass.equals(Double.class)) {
			return val.doubleValue();
		} else if (fieldClass.equals(Integer.class)) {
			return val.intValue();
		} else if (fieldClass.equals(Float.class)) {
			return val.floatValue();
		} else if (fieldClass.equals(Long.class)) {
			return val.longValue();
		} else if (fieldClass.equals(Short.class)) {
			return val.shortValue();
		} else if (fieldClass.equals(Byte.class)) {
			return val.byteValue();
		} else {
			return 0;
		}
	}
}
