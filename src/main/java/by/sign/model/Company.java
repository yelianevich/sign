package by.sign.model;

import java.util.ArrayList;
import java.util.List;

import by.sign.model.mapper.MapperModel;
import by.sign.model.mapper.Prop;

@MapperModel
public class Company {

	@Prop("_id")
	private String companyId;

	@Prop
	private String name;

	@Prop
	private String industry;

	private List<ServiceUser> users = new ArrayList<>();

	@Prop
	private String country;

	@Prop
	private String city;

	@Prop
	private Integer maxUsers;

	public Company() {
	}

	public Company(String companyId, String name) {
		this.companyId = companyId;
		this.name = companyId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public List<ServiceUser> getUsers() {
		return users;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setUsers(List<ServiceUser> users) {
		this.users = users;
	}

	public Integer getMaxUsers() {
		return maxUsers;
	}

	public void setMaxUsers(Integer maxUsers) {
		this.maxUsers = maxUsers;
	}

	@Override
	public String toString() {
		return "Company [companyId=" + companyId + ", name=" + name + ", industry=" + industry
				+ ", users=" + users + ", maxUsers=" + maxUsers + "]";
	}
}
