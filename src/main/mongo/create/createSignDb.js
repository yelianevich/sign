db.user.drop();
db.company.drop();

// x - private key
// y - public key
var companyCreation = function(companyName, industry, country, city, 
		userName, firstName, lastName, x, y,
		anotherUserName, anotherFirstName, anotherLastName, ax, ay) {
	var companyId = new ObjectId();
	var user = {
		_id: userName,
		password:"p",
		roles: ["ROLE_USER"],
		email: userName + "@email.com",
		firstName : firstName,
		lastName : lastName,
		position : "Manager",
		companyid : companyId,
		x : x,
		y : y
	};
	db.user.insert(user);
	var anotherUser = {
		_id: anotherUserName,
		password:"p",
		roles: ["ROLE_USER"],
		email: userName + "@email.com",
		firstName : anotherFirstName,
		lastName : anotherLastName,
		position : "Employee",
		companyid: companyId,
		x : ax,
		y : ay
	};
	db.user.insert(anotherUser);
	var company = {
		_id : companyId,
		name : companyName,
		industry: industry,
		country: country,
		city: city,
		users: [user._id, anotherUser._id],
		maxUsers: 5
	};
	db.company.insert(company);
};

// companies creation
companyCreation("BSUIR", "Education", "Belarus", "Minsk", 
		"ubsuir", "Ivan", "Ivanov", "13f9ae9e000d9e7c941bd004b891023ff0f29efb", "6add9cc06732f4db7bd58eb6771850ee2347d9a5192a4a8a7d42dde1dc3943edaa6f5768e787f14a4c84ca344e71be2f91446fc17b62c33969d6eacf9ffcefd460f4fb7fded05f2cb3c7a9b2488c36181ae4d81f774675d5ef598d412791c3af24e1f2a64a49530282167090c08460c5b2e52e117ac665b8600967bc7f315fe5",
		"absuir", "Petr", "Petrov", "ad6f525bcc8edf335360da652e62a0a420827299", "69206e419db5d98ed5b0ceb25db507d22aa62d2bb314f3f6e7b4dc2bca9f359a8ae3a68472200c0e834c0302e1ef7eceda054878377bf9f1ad284d7b6a1a994cd7385659b984dcd8f243c5f10838debf7908e7ab9c81143515b1e0038ffb4c635021ba1c2357d80324677a6f31f716214ee0cc59086512133d3c3145516681d4"
		);
companyCreation("BSU", "Education", "Belarus", "Minsk", 
		"ubsu", "Ivan", "Ivanov", "980280486a20ea3a664d4185bf3d0e935d3fb7d8", "26334ea236b82471968f6df191de66f5d848a72b44416156319823dfbeb6e3e73777ffc518a9f6f4b359e99db68343226555957e39c3a92f688fe614d308c319a76e05ee11546463921f5b6452245397120c47332625d77a0f4f44efb25947125061436de5cc217617bd9743b6c02c33d2e3d536bd2bf3e13b18091d62d4104",
		"absu", "Petr", "Petrov", "8f77e2526d0183acb05d04882e4655366d863902", "ba0797797dc6ccf86341429e589c0bcf2f45f7b6023206b2f17f04c0fe3a2bafdebeecaeeb1433cbd594eafa67698ed2c553cfae325384ed17040991e6801ce56aac7171976d6c1512b05331cdb48b2c9702975cad28ba724f2c6fbd3dfcad59db2f3b46a858f8cc41c40c2212438b5d46b7ad51e1ca8b46a08b30ac6fc40d8b"
		);
companyCreation("BNTU", "Education", "Belarus", "Minsk", 
		"ubntu", "Ivan", "Ivanov", "9bb24daef369ea48558ee57923e95d8996a279c", "64e2bc1579fd4565172d463c727cd8e1f5101236b7440df1e425420db96de2c0658e3cd84cdcb57652ca2e44a385a55c49694b9b689f8e9437f570527e1222745c6a26d3424f32b5268bab4849ef25e478e48dbd9a91e531d5cb527e4d4280203d5b70e0ea9319e2fcd694feb7510939f80cf8891dd891ac858307e40d2982d6",
		"abntu", "Petr", "Petrov", "3dae4f3e331acce52f147343a0b7134e62abf857", "80c487c229a9e9ada5680b9ebd586eda85054fa83a2b23355267c282e36e97fb7cf5063d28c751c016c7ad80bddc790e7dc1906319e4a60cdae969d0382d735758bbb65c4a8309879f2dc407c725240082a60d0d7d6d923956085b652cc1a16d029df69cbd489f9e64878b20cfd55c9e3f9cbc7340f7a8c9bbb63bede2d4d635");
companyCreation("EPAM Systems", "IT", "USA", "New Town", 
		"uepam", "Ivan", "Ivanov", "b434658c2c88abc1f1ca6eff59f02bf2a066d4f1", "25f11344cf6f22cb807a9528236e67981d6c7a4153cf0a058d8dbc34c9e1878ab037bf04fd6898edc2a7b53ab69372418c48104dec58030405532453278aa24b099b966cd919ea753965c200a953e3db97b149f26658b9d776c6af97f22ca8876bf30005c297275d16f8f6cd31c5ef2ce952e2b64bbab657256751ffd0c44cd7",
		"aepam", "Petr", "Petrov", "40823bc05b00808327c0c9217ed5aaeed0bf85f8", "7381391988b27f9c7c3df5cc06bd593fcac5b9745f78d2cc5732a9526ad5c2fef6322492784d34880dddc5c5f9fc1c591d4244fc12065d0cefb1c31a162765dd5b1e38481b6e18c1b95ab1568410874a1b9afb3155b920142287c55d5bd6d7fc9b3e8688c1a8aeb202432b5ab0a7f2f89b94619337cf0fa35049bb09ac87e8dc"
		);
companyCreation("isSoft", "IT", "Belarus", "Minsk", 
		"uissoft", "Ivan", "Ivanov", "45ad6f2a65a21a1e29a5983d2ab5e30f66aeb8f", "b61efe9517a1c600a8386f4c489d69179a9b3555ba1cd6541b4f386bb4ed1913c0bc9854777fd1f28b2b38ea756fa2672912223fe5a22a8e8b366b65b9ab696a734f28bb44e28acf6d98cfb83e5fdc357782ab3833282494115dffa3c75bbe4ddca85718b09443c5b02b3d8a6c2d7baa8b2b3a314169cc5ccb73b1d17f296df1",
		"aissoft", "Petr", "Petrov", "3e65c7449c63c46c3a602927c12d2ec4a78d36b4", "18ef1468e04e07f81995db9c1225001a84e9baf68cc70166c708db951cac726913b59775c887ea6864cda949d1db70d3a21cac4327f026f7f29a4ee179fb9c45fbbf6e038ac18d112e4b0e12cfe6059c8e585a082db8e597b623592382c6f7aeb698b1ec3b64f28a4c229ed7c074c6c825efe36cf09c6c1a75657dcfe276861"
		);

var serviceAdmin = {
	_id: "a",
	password:"p",
	roles: ["ROLE_ADMIN"],
	email: "admin@email.com",
	firstName : "Raman",
	lastName : "Yelianevich",
	position : "Admin",
};
db.user.insert(serviceAdmin);

db.user.ensureIndex({"_id" : 1, "unique" : true});
db.user.ensureIndex({"y" : 1, "unique" : true});
